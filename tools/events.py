from csnake import CodeWriter, Struct, Variable, FuncPtr, Function
from pathlib import Path
from textwrap import dedent
import subprocess

events = {
    "Key": {
        "members": [
            {"type": "CgeButton", "name": "button"},
            {"type": "bool", "name": "pressed"},
            {"type": "bool", "name": "repeat"},
        ],
        "includes": ["cge/button.h"],
    },
    "MouseButton": {
        "members": [
            {"type": "CgeButton", "name": "button"},
            {"type": "bool", "name": "pressed"},
        ],
        "includes": ["cge/button.h"],
    },
    "MouseScroll": {
        "members": [
            {"type": "CgeVec2f", "name": "offset"},
        ],
        "includes": ["cge/button.h"],
    },
    "MouseMoved": {
        "members": [
            {"type": "CgeVec2f", "name": "position"},
        ],
        "includes": ["cge/button.h"],
    },
    "WindowResize": {
        "members": [{"type": "CgeVec2i", "name": "size"}],
        "includes": [],
    },
    "WindowClose": {
        "members": [],
        "includes": [],
    },
}
"""
define data type
forward declare event type
define callback function
define event type

functions
	- register cb
	- construct
	- get data fields
"""
from re import sub


def snake_case(s) -> str:
    return "_".join(
        sub(
            "([A-Z][a-z]+)", r" \1", sub("([A-Z]+)", r" \1", s.replace("-", " "))
        ).split()
    ).lower()


def get_git_root(path):
    git_root = subprocess.check_output(
        ["git", "rev-parse", "--show-toplevel"], cwd=path
    )
    return git_root.decode("utf-8").strip()


root_dir = Path(get_git_root("."))
event_include_dir = root_dir / "include/cge/event"
event_source_dir = root_dir / "src/cge/event"

base_type = Struct(f"CgeEventBase", typedef=True)


if __name__ == "__main__":
    types = []
    for event_name, event_info in events.items():
        functions = []
        cw_header = CodeWriter()
        header_guard = "CGE_" + snake_case(event_name).upper() + "_EVENT_H_"
        cw_header.start_if_def(header_guard, True)
        cw_header.add_define(header_guard)
        cw_header.include("cge/core.h")
        cw_header.include("cge/common/pch.h")
        cw_header.include("cge/common/vec.h")
        cw_header.include("cge/event/event_base.h")
        for include in event_info["includes"]:
            cw_header.include(include)
        data_type = Struct(f"Cge{event_name}Data", typedef=True)
        event_type = Struct(f"Cge{event_name}Event", typedef=True)
        """ Create data type  """
        event_type.add_variable(Variable("base", base_type.name))
        event_type.add_variable(Variable("data", data_type.name))
        """ Create callback function type """
        for members in event_info["members"]:
            name = members["name"]
            type = members["type"]
            data_type.add_variable(Variable(name, type))
        cw_header.add_struct(data_type)
        cw_header.add_struct(event_type)
        callback_type = Struct(f"Cge{event_name}EventCallback")
        event_cb = Variable("registeredEventHandler", primitive=callback_type.name)

        callback_func = (
            f"\ntypedef void (* {callback_type.name})({event_type.name} *);\n"
        )
        cw_header.add(callback_func)

        cb_wrapper_func = Function(
            "eventCallbackWrapper",
            "void",
            "static",
            [("*handler", "CgeEventHandler_t")],
        )
        cb_wrapper_code = (
            f"if(handler == NULL){{ "
            f'CGE_ASSERT_NOT_NULL(handler, "Event handler is NULL!");'
            f"return;}}"
            f"if ( {event_cb.name}!= NULL) {{"
            f"\n\tCgeEventBase *base = cgeEventContainerOf(handler);"
            f"\n\t{event_type.name} *self ="
            f"\n\t\tCONTAINER_OF(base, {event_type.name}, base);"
            f"\n\t{event_cb.name}(self);"
            f"\n}}"
        )
        cb_wrapper_func.add_code(dedent(cb_wrapper_code))

        register_cb_func = Function(
            f"cge{event_name}EventRegisterCb",
            return_type="CgeStatus",
            arguments=[("callback", callback_type.name)],
        )
        functions.append(register_cb_func)
        register_cb_code = (
            f"if(callback == NULL){{"
            f'CGE_ASSERT_NOT_NULL(callback, "Callback is NULL!");'
            f"return CGE_FAIL;"
            f"}}"
            f"{event_cb.name} = callback;"
            f"return CGE_OK;"
        )
        register_cb_func.add_code(register_cb_code)

        init_func = Function(
            f"cge{event_name}EventConstruct",
            return_type=event_type.name,
            arguments=data_type.variables,
        )
        init_func.add_code(
            f'CGE_ASSERT_NOT_NULL({event_cb.name}, "No event handler registered!");'
        )
        init_func.add_code(f"{event_type.name} self;")
        init_func.add_code(f"cgeEventInit(&self.base, eventCallbackWrapper);")
        for member in data_type.variables:
            init_func.add_code(f"self.data.{member.name} = {member.name};")

        init_func.add_code("return self;")
        functions.append(init_func)

        for member in data_type.variables:
            getter_func = Function(
                f"cge{event_name}EventGet{member.name.title()}",
                return_type=member.primitive,
                arguments=[("*self", event_type.name)],
            )
            getter_func.add_code(
                f'CGE_ASSERT_NOT_NULL(self, "Self is NULL!");'
                f"return self->data.{member.name};"
            )
            functions.append(getter_func)

        file_name_header = snake_case(f"{event_name}Event.h")
        file_name_source = snake_case(f"{event_name}Event.c")
        file_path_header = event_include_dir / file_name_header
        file_path_source = event_source_dir / file_name_source

        """ SOurce file """
        cw_source = CodeWriter()
        cw_source.include(f"cge/event/{file_name_header}")

        cw_source.add(f"\nstatic {event_cb.primitive} {event_cb.name} = NULL;")
        cw_source.add_function_definition(cb_wrapper_func)

        for func in functions:
            cw_header.add_function_prototype(func)
            cw_source.add_function_definition(func)

        cw_header.end_if_def()

        cw_header.write_to_file(file_path_header)
        cw_source.write_to_file(file_path_source)

        print(f"Wrote files {file_path_header}")
        print(f"Wrote files {file_path_source}")
