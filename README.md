# CGE: C Growth engine
A 2D game engine, created for the sole purpuse of growing my C knowledge.

## Docker Runner
### Build and deploy new container
```
# Get container ID
docker container ls
docker container stop <contaier_id>
docker container rm <contaier_id>

docker build -f ./.docker/Dockerfile -t builder-bob:latest .

# Get new image ID
docker images

# Deploy new image
docker run -d --name cutom-gitlab-runner --restart always \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v /srv/gitlab-runner/config:/etc/gitlab-runner \
        <image_id>

```

### Run pipeline locally
```
# build docker image
docker build -f ./.docker/Dockerfile -t builder-bob:latest .

# run pipeline stage
gitlab-runner exec docker --docker-pull-policy=never <build_stage>
```
