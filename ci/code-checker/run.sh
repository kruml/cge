#!/usr/bin/env bash

if [[ ! -z "$CI_PROJECT_DIR" ]]; then
    echo "Running codechecker in gitlab-ci!"
    root_dir="$CI_PROJECT_DIR"
else
    root_dir=`git rev-parse --show-toplevel`
fi

code_checker_dir=$root_dir/ci/code-checker
output_dir=$code_checker_dir/output

rm -rf $output_dir && mkdir $output_dir

# Log your project.
CodeChecker log -b " 	        \
	cd $root_dir 	&&          \
    ./scripts/build.sh -c" 	    \
    -o $output_dir/compilation_database.json

# Analyze your project.
CodeChecker analyze -c \
    -i $code_checker_dir/skip-file \
    -o $output_dir/reports \
    $output_dir/compilation_database.json

# Create the report file by using the CodeChecker parse command.
CodeChecker parse \
    --trim-path-prefix $(pwd) \
    -e codeclimate \
    $output_dir/reports > $output_dir/gl-code-quality-report.json

CodeChecker parse -e html $output_dir/reports/ -o $output_dir/reports_html

# Exit with status code 1 if there is any report in the output file.
status=$(cat $output_dir/gl-code-quality-report.json)
if [[ -n "$status" && "$status" != "[]" ]]; then
    echo "Found errors in quality report!"
    exit 1
fi
