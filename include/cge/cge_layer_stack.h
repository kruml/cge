#ifndef CGE_LAYER_STACK_H
#define CGE_LAYER_STACK_H

#include "cge_layer.h"

#define MAX_LAYERS (10)

#define for_each_layer(stack, end_idx)                         \
        CgeLayerIterator iter = cge_lstack_begin(stack);       \
        CgeLayerIterator end = cge_lstack_end(stack, end_idx); \
        for (; iter <= end; iter = cge_lstack_next(iter))

typedef struct CgeLayerStack cge_layerstack_t;

typedef cge_layer_t *CgeLayerIterator;

cge_layerstack_t *cge_lstack_new(void);
void cge_lstack_destroy(cge_layerstack_t *stack);

bool cge_lstack_push(cge_layerstack_t *stack, cge_layer_t layer);
bool cge_lstack_pop(cge_layerstack_t *stack);
bool cge_lstack_push_overlay(cge_layerstack_t *stack, cge_layer_t layer);
bool cge_lstack_pop_overlay(cge_layerstack_t *stack);

CgeLayerIterator cge_lstack_begin(cge_layerstack_t *stack);
CgeLayerIterator cge_lstack_end(cge_layerstack_t *stack, int end_idx);
CgeLayerIterator cge_lstack_next(CgeLayerIterator iter);
void cge_lstack_update(cge_layerstack_t *layers, f32 dt_seconds);

#endif // CGE_LAYER_STACK_H
