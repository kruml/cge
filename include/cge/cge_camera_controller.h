#ifndef CGE_ORTHOCAM_CONTROLLER_H_
#define CGE_ORTHOCAM_CONTROLLER_H_

#include "cge/cge_camera.h"
#include "cge/cge_layer.h"

struct cge_camortho_ctrl {
        struct cge_camortho camera;
        vec3 velocity;
        float move_speed;
        float zoom_speed;
        float rotation_speed;
};

void cge_camortho_ctrl_cfg(const struct cge_camortho_ctrl *config);
const struct cge_camortho *cge_camortho_ctrl_get_cam(void);
void cge_camortho_ctrl_attach(void);
void cge_camortho_ctrl_update(f32 dt_seconds);

static const cge_layer_t camortho_ctrl_layer = { .on_attach = cge_camortho_ctrl_attach,
                                                 .on_update = cge_camortho_ctrl_update };

#endif //CGE_ORTHOCAM_CONTROLLER_H_
