#ifndef CGE_ASSERT_
#define CGE_ASSERT_

#include "cge_pch.h"

#ifdef NDEBUG
#define CGE_ASSERT(condition, message) ((void)0)
#define CGE_ASSERT_NOT_NULL(condition, message) ((void)0)
#else
#define CGE_ASSERT(condition, message)                                                                        \
        do {                                                                                                  \
                if (!(condition)) {                                                                           \
                        fprintf(stderr, "%s:%d: Assertion `%s` failed: %s\n", __FILE__, __LINE__, #condition, \
                                (message));                                                                   \
                        abort();                                                                              \
                }                                                                                             \
        } while (0)

#define CGE_ASSERT_NOT_NULL(condition, message)                                                               \
        do {                                                                                                  \
                if ((condition) == NULL) {                                                                    \
                        fprintf(stderr, "%s:%d: Assertion `%s` failed: %s\n", __FILE__, __LINE__, #condition, \
                                (message));                                                                   \
                        abort();                                                                              \
                }                                                                                             \
        } while (0)

#define CGE_ASSERT_EVENT(expected, actual) CGE_ASSERT(actual == expected, "Received wrong event!");
#endif

#define CGE_ASSERT_ALWAYS(condition, message)                                                                 \
        do {                                                                                                  \
                if (!(condition)) {                                                                           \
                        fprintf(stderr, "%s:%d: Assertion `%s` failed: %s\n", __FILE__, __LINE__, #condition, \
                                (message));                                                                   \
                        abort();                                                                              \
                }                                                                                             \
        } while (0)
#endif // CGE_ASSERT_
