
#ifndef CGE_STATUS_
#define CGE_STATUS_

typedef enum { CGE_OK = 0, CGE_FAIL = -1, CGE_PARAM_ERROR = -2 } cge_status_t;

#endif // CGE_STATUS_
