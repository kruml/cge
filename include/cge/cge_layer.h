#ifndef CGE_LAYER_H
#define CGE_LAYER_H

#include "cge/cge_core.h"

typedef void (*cge_layer_attach_fn)(void);
typedef void (*cge_layer_detach_fn)(void);
typedef void (*cge_layer_update_fn)(f32 dt_seconds);

typedef struct CgeLayer {
        cge_layer_attach_fn on_attach;
        cge_layer_detach_fn on_detach;
        cge_layer_update_fn on_update;
} cge_layer_t;

#endif // CGE_LAYER_H
