#ifndef CGE_RENDERER_H
#define CGE_RENDERER_H

#include <cglm/cglm.h>

#include "cge/cge_buffer.h"
#include "cge/cge_camera.h"
#include "cge/cge_shader.h"
#include "cge/cge_texture.h"

void cge_renderer_init(void);
void cge_renderer_begin_scene(const struct cge_camortho *camera);
void cge_renderer_end_scene(void);
void cge_renderer_draw_quad2d(vec2 position, vec2 size, vec4 color);
void cge_renderer_draw_quad3d(vec3 position, vec2 size, vec4 color);
void cge_renderer_draw_quad2d_tex(vec2 position, vec2 size, const struct cge_texture2d *texture);
void cge_renderer_draw_quad3d_tex(vec3 position, vec2 size, const struct cge_texture2d *texture);
void cge_renderer_set_clear_color(const vec4 color);
void cge_renderer_clear(void);
void cge_renderer_on_winresize(u32 width, u32 height);

#endif //CGE_RENDERER_H
