#ifndef CGE_APP_H
#define CGE_APP_H

#include "cge/cge_layer_stack.h"
#include "cge/cge_window.h"
#include "cge/cge_status.h"

#include "ecs/decs.h"

struct cge_settings {
        int window_w;
        int window_h;
        bool window_vsync;
        bool window_fullscreen;
        const char *window_title;
};

// Handle systems thorugh ap
// - Will start bloating the app interface
// Handle it separately
// - User will need access to most of the interface
// - App needs access to the ecs update/run functions.
// - This would be similar to how the renderer works.

cge_status_t cge_init(const struct cge_settings *settings);
cge_status_t cge_push_layer(cge_layer_t layer);
cge_status_t cge_push_overlay(cge_layer_t layer);
cge_status_t cge_pop_layer(void);
int cge_get_window_h(void);
int cge_get_window_w(void);
cge_window_t *cge_get_window(void);
void cge_run(void);

#endif // CGE_APP_H
