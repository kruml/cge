#ifndef CGE_CORE_H
#define CGE_CORE_H

#include "cge/cge_pch.h"
#include "cge/cge_log.h"
#include "cge/cge_assert.h"
#include <stdlib.h>

#include <assert.h>
#include <stddef.h>
#include <time.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float f32;
typedef double f64;

typedef ptrdiff_t size;
typedef size_t usize;

typedef const char *cstr;

/* --- General Macros ----------------------------------------------------------------*/
#define advance_ptr(ptr, n, max) \
        ((((ptr) + (n)) <= (max)) ? ((ptr += n), 1) : (assert(!"Pointer advance out of bounds!"), 0))

#define max(a, b) ((a) > (b)) ? a : b

#define countof(a) (ptrdiff_t)(sizeof(a) / sizeof(*(a)))

#define lengthof(s) (countof(s) - 1)

#define is_bit_set(var, n) (((var >> n) & 0x01) == 0x01)

#define IS_FLAG_SET(flag, mask) (((flag) & (mask)) == (flag))

#define CONTAINER_OF(ptr, type, member) ((type *)((char *)(ptr)-offsetof(type, member)))

#define CHECK(expr)                                                                    \
        do {                                                                           \
                cge_status_t retval = expr;                                            \
                if (retval < 0) {                                                      \
                        CGE_LOG_DEBUG("Runtime error: %s returned %d", #expr, retval); \
                        return retval;                                                 \
                }                                                                      \
        } while (0);

/* --- Time Step ---------------------------------------------------------------------*/
#define MS_IN_SEC (1000)
#define SEC_TO_MS(sec) (sec / MS_IN_SEC)
#define MS_TO_SEC(ms) (ms * MS_IN_SEC)

/* --- Memory ------------------------------------------------------------------------*/
typedef void (*malloc_errhandler)(usize);
typedef void *(*malloc_impl)(usize);

static void default_malloc_errhandler(usize size);
static inline void *default_malloc_impl(usize size);

static malloc_errhandler mallocerr = default_malloc_errhandler;
static malloc_impl mallocimpl = default_malloc_impl;

static void default_malloc_errhandler(usize size)
{
        CGE_LOG_ERROR("Fatal runtime error: failed allocating %zu bytes of memory", size);
        CGE_ASSERT_ALWAYS(false, "Fatal runtime error: failed malloc");
        abort();
}

static inline void *default_malloc_impl(usize size)
{
        void *ptr = malloc(size);
        if (ptr == NULL)
                mallocerr(size);

        return ptr;
}

#define cge_malloc(type, count) mallocimpl(sizeof(type) * count)
#define cge_free(ptr)      \
        if (ptr) {         \
                free(ptr); \
        }

#endif // CGE_CORE_H
