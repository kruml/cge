#ifndef CGE_SHADER_H
#define CGE_SHADER_H

#include <cglm/cglm.h>

typedef struct cge_shader cge_shader;

/**
 * Reads specified shaderfile, parses and compiles the shaders.
 * Absolute path to assets folder set with define DEFAULT_ASSETS_PATH in build system.
 *
 * @todo: Override DEFAULT_ASSETS_PATH with env varible
 *
 * @param filepath relative path to shader file
 * @return shader
 */
cge_shader *cge_shader_create_from_file(const char *filepath);
cge_shader *cge_shader_create(const char *vertex_src, const char *fragment_src);
void cge_shader_destroy(cge_shader *self);
void cge_shader_bind(const cge_shader *self);
void cge_shader_unbind(void);
void cge_shader_set_int(const cge_shader *self, const char *name, int num);
void cge_shader_set_mat4(const cge_shader *self, const char *name, const mat4 matrix);
void cge_shader_set_vec3(const cge_shader *self, const char *name, const vec3 uniform);
void cge_shader_set_vec4(const cge_shader *self, const char *name, const vec4 uniform);

#endif //CGE_SHADER_H
