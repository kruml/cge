#ifndef CGE_TEXTURE_H
#define CGE_TEXTURE_H

#include "cge/cge_core.h"

struct cge_texture2d {
        i32 width;
        i32 height;
        u32 renderer_id;
        char *path;
};

struct cge_texture2d cge_texture_create(const char *path);
void cge_texture_destroy(struct cge_texture2d *self);
void cge_texture_bind(const struct cge_texture2d *self, u32 slot);

#endif //CGE_TEXTURE_H
