#ifndef CGE_INPUT_H
#define CGE_INPUT_H

#include "cge/cge_keymap.h"

#include <stdbool.h>

void cge_input_init(void);
bool cge_input_is_key_pressed(enum cge_key key);

#endif //CGE_INPUT_H
