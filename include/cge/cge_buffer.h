#ifndef CGE_RENDERER_BUFFER_H

#include "cge/cge_assert.h"
#include "cge/cge_core.h"
#include "cge/cge_log.h"
#include "cge/cge_status.h"

#include <glad/glad.h>

/* ---- Shader Data Type ---------------------------------------------------- */

/**
 * @brief Macro for defining and generating code for shader data type
 *
 * Format: X(ENUM_NAME, size, component_count, opengl_type)
 */

#define SHADER_DATA_TYPE                                        \
        X(CGE_SHADER_DATA_NONE, 0, 0, 0)                        \
        X(CGE_SHADER_DATA_FLOAT, 4, 1, GL_FLOAT)                \
        X(CGE_SHADER_DATA_VEC2, (4 * 2), 2, GL_FLOAT)           \
        X(CGE_SHADER_DATA_VEC3, (4 * 3), 3, GL_FLOAT)           \
        X(CGE_SHADER_DATA_VEC4, (4 * 4), 4, GL_FLOAT)           \
        X(CGE_SHADER_DATA_MAT3, (4 * 3 * 3), (3 * 3), GL_FLOAT) \
        X(CGE_SHADER_DATA_MAT4, (4 * 3 * 4), (4 * 4), GL_FLOAT) \
        X(CGE_SHADER_DATA_INT, 4, 1, GL_INT)                    \
        X(CGE_SHADER_DATA_INT2, (4 * 2), 2, GL_INT)             \
        X(CGE_SHADER_DATA_INT3, (4 * 3), 3, GL_INT)             \
        X(CGE_SHADER_DATA_INT4, (4 * 4), 4, GL_INT)             \
        X(CGE_SHADER_DATA_BOOL, 1, 1, GL_BOOL)

typedef enum cge_shader_data_type_t {
#define X(name, size, component_count, opengl_type) name,
        SHADER_DATA_TYPE
#undef X
} cge_shader_data_type_t;

static inline GLenum cge_shader_data_type_to_opengl(cge_shader_data_type_t type)
{
        switch (type) {
#define X(name, size, component_count, opengl_type) \
        case name: return opengl_type;
                SHADER_DATA_TYPE
#undef X
        }
        CGE_LOG_DEBUG("Unknown shader data type: %d", type);
        CGE_ASSERT(false, "");
        return 0;
}

static uint32_t cge_shader_data_type_size(cge_shader_data_type_t type)
{
        switch (type) {
#define X(name, size, component_count, opengl_type) \
        case name: return size;
                SHADER_DATA_TYPE
#undef X
        }

        CGE_ASSERT(false, "Unknown ShaderDataType!");
        return 0;
}

/* ---- Buffer Element------------------------------------------------------- */

typedef struct cge_bufferelement_t {
        cge_shader_data_type_t data_type;
        char *name;
        u32 size;
        size_t offset;
        bool normalized;
} cge_bufferelement_t;

static inline cge_bufferelement_t cge_bufferelement_create(cge_shader_data_type_t data_type, char *name)
{
        return (cge_bufferelement_t){
                .data_type = data_type, .name = name, .size = cge_shader_data_type_size(data_type), .normalized = false
        };
}

static inline u32 cge_bufferelement_component_count(cge_bufferelement_t *element)
{
        switch (element->data_type) {
#define X(name, size, component_count, opengl_type) \
        case name: return component_count;
                SHADER_DATA_TYPE
#undef X
        }

        CGE_ASSERT(false, "Unkown element data type!");
        return 0;
}

/* ---- Buffer Layout------------------------------------------------------- */

typedef struct cge_bufferlayout cge_bufferlayout_t;
typedef cge_bufferelement_t *cge_bufferlayout_iter_t;

cge_bufferlayout_t *cge_bufferlayout_new(cge_bufferelement_t *elements, u32 count);
void cge_bufferlayout_destroy(cge_bufferlayout_t *layout);
u32 cge_bufferlayout_stride(const cge_bufferlayout_t *layout);
cge_bufferlayout_iter_t cge_bufferlayout_start(const cge_bufferlayout_t *layout);
cge_bufferlayout_iter_t cge_bufferlayout_end(const cge_bufferlayout_t *layout);
cge_bufferlayout_iter_t cge_bufferlayout_next(cge_bufferlayout_iter_t iter);

#define for_each_bufferelement(layout, end_idx)                        \
        cge_bufferlayout_iter_t iter = cge_bufferlayout_start(layout); \
        cge_bufferlayout_iter_t end = cge_bufferlayout_end(layout);    \
        for (; iter <= end; iter = cge_bufferlayout_next(iter))

/* ---- Vertex Buffer ------------------------------------------------------- */

typedef struct cge_vertexbuf cge_vbuf_t;

cge_vbuf_t *cge_vbuf_new(f32 *vertices, i32 size);
void cge_vbuf_destroy(cge_vbuf_t *buf);
void cge_vbuf_bind(const cge_vbuf_t *buf);
void cge_vbuf_unbind(void);
void cge_vbuf_set_layout(cge_vbuf_t *buf, cge_bufferlayout_t *layout);
cge_bufferlayout_t *cge_vbuf_get_layout(const cge_vbuf_t *buf);

/* ---- Index Buffer ------------------------------------------------------- */

typedef struct cge_indexbuf cge_ibuf_t;

cge_ibuf_t *cge_ibuf_new(u32 *indices, u32 count);
void cge_ibuf_destroy(cge_ibuf_t *buf);
void cge_ibuf_bind(const cge_ibuf_t *buf);
void cge_ibuf_unbind(void);
u32 cge_ibuf_count(const cge_ibuf_t *buf);

/* ---- Vertex Array ------------------------------------------------------- */

typedef struct cge_vertexarray cge_varray_t;

cge_varray_t *cge_varray_new(void);
void cge_varray_destroy(cge_varray_t *buf);
void cge_varray_bind(const cge_varray_t *buf);
void cge_varray_unbind(void);
cge_status_t cge_varray_add_vbuf(cge_varray_t *array, cge_vbuf_t *buf);
cge_status_t cge_varray_set_ibuf(cge_varray_t *array, cge_ibuf_t *buf);
cge_ibuf_t *cge_varray_get_ibuf(const cge_varray_t *array);

#define CGE_RENDERER_BUFFER_H
#endif //CGE_RENDERER_BUFFER_H
