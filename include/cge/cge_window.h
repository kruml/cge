#ifndef CGE_WINDOW_H
#define CGE_WINDOW_H

#include "cge/cge_status.h"

#include <stdbool.h>

typedef struct cge_window cge_window_t;

cge_window_t *cge_win_new(void);
cge_status_t cge_win_init(cge_window_t *window, int height, int width, bool vsync, bool fullscreen, const char *title);

int cge_win_height(cge_window_t *window);
int cge_win_width(cge_window_t *window);
void cge_win_update(cge_window_t *window);
void cge_win_destroy(cge_window_t **window);
void *cge_win_native(cge_window_t *window);

#endif // CGE_WINDOW_H
