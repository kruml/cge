#ifndef CGE_CAMERA_H
#define CGE_CAMERA_H

#include <cge/cge_core.h>

#include <cglm/cglm.h>

/**
 * @struct cge_camortho
 * @brief Orthographic Camera struct
 *
 * This structure represents a ortographic camera to be used for rendering scenes.
 *
 * After modifying @ref position and/or @ref rotation, @ref cge_camortho_update_view_transform
 * must be called to updated the matrices. Matrices should not be modified manually.
 */
struct cge_camortho {
        mat4 projection_matrix; /* !< Should not be modified. Calculated via @ref cge_camortho_update_view_transform */
        mat4 view_matrix; /* !< Should not be modified. Calculated via @ref cge_camortho_update_view_transform */
        mat4 view_projection_matrix; /* !< Should not be modified. Calculated via @ref cge_camortho_update_view_transform */
        vec3 position; /* !< x,y,z position as float */
        f32 rotation; /* !< Rotation in degrees */
        f32 orthosize;
};

struct cge_camortho cge_camortho_init(f32 width, f32 height);

void cge_camortho_update_projection(struct cge_camortho *self, f32 width, f32 height);
void cge_camortho_update_view_transform(struct cge_camortho *self);

#endif //CGE_CAMERA_H
