/**
 * @file cge/cge_events.h
 * @brief Defines event types with data, and functions for pub/sub events
 */

#ifndef CGE_EVENTS_H
#define CGE_EVENTS_H

#include "cge/cge_keymap.h"

#include <cglm/cglm.h>
#include <stdbool.h>

enum cge_event_id {
        CGE_KEYEVENT = 0x0001,
        CGE_MOUSEMOTION = 0x0002,
        CGE_MOUSEBUTTON = 0x0004,
        CGE_MOUSESCROLL = 0x0008,
        CGE_WINDOWCLOSED = 0x0010,
        CGE_WINDOWRESIZE = 0x0020,
        CGE_EVENTCNT = CGE_WINDOWRESIZE, /* Increment on added events */
};

struct cge_keypress {
        enum cge_key key;
        bool pressed;
        bool repeat;
};

struct cge_mousepress {
        enum cge_mousebutton button;
        bool pressed;
};

struct cge_mousescroll {
        vec2 offset;
};

struct cge_mousemotion {
        vec2 position;
};

struct cge_windowresize {
        vec2 size;
};

struct cge_event {
        enum cge_event_id id;
        union {
                struct cge_keypress keypress;
                struct cge_mousepress mousepress;
                struct cge_mousescroll mousescroll;
                struct cge_mousemotion mousemotion;
                struct cge_windowresize windowresize;
        };
};

/**
 * @defgroup event_init Event Init Macros
 * @brief Helper macros for initializing cge_event using compound litteral. Can be used with @see @ref cge_event_publish()
 * @{
 */

#define MOUSEPRESS_EVENT(button_val, pressed_val)                                                    \
        (struct cge_event)                                                                           \
        {                                                                                            \
                .id = CGE_MOUSEBUTTON, .mousepress = {.button = button_val, .pressed = pressed_val } \
        }

#define KEYPRESS_EVENT(key_val, pressed_val, repeat_val)                                                        \
        (struct cge_event)                                                                                      \
        {                                                                                                       \
                .id = CGE_KEYEVENT, .keypress = {.key = key_val, .pressed = pressed_val, .repeat = repeat_val } \
        }

#define MOUSESCROLL_EVENT(x, y)                                                           \
        (struct cge_event)                                                                \
        {                                                                                 \
                .id = CGE_MOUSESCROLL, .mousescroll = {.offset = { (float)x, (float)y } } \
        }

#define MOUSEMOTION_EVENT(x, y)                                                             \
        (struct cge_event)                                                                  \
        {                                                                                   \
                .id = CGE_MOUSEMOTION, .mousemotion = {.position = { (float)x, (float)y } } \
        }

#define WINDOWRESIZE_EVENT(width, height)                                                          \
        (struct cge_event)                                                                         \
        {                                                                                          \
                .id = CGE_WINDOWRESIZE, .windowresize = {.size = { (float)width, (float)height } } \
        }

#define WINDOWCLOSED_EVENT             \
        (struct cge_event)             \
        {                              \
                .id = CGE_WINDOWCLOSED \
        }

/** @} */ // end of init_macro group

typedef void (*cge_event_listener)(const struct cge_event *);

/**
 * @brief Register event listener to event
 *
 * @param type Event to listen to. Can handle several masked values.
 * @param listener Event listener callback
 *
 * @example
 * static void example_listener(const struct cge_event * event)
 * {
 *      switch(event->id){
 *      case CGE_WINDOWRESIZE: {
 *          const struct cge_windowresize* event_data = &event->windowresize;
 *          ...
 *          ...
 *          break;
 *      }
 *      }
 * }
 *
 * cge_event_subscribe((CGE_WINDOWRESIZE | CGE_WINDOWCLOSED), example_listener);
 */
void cge_event_subscribe(enum cge_event_id type, cge_event_listener listener);

/**
 * @brief Publish an event to the event queue. Event is copied by value.
 * There are compound litteral hepler macros for each event type. @see @ref event_init.
 *
 * @param event Pointer to event. Event handler takes ownership by copying event.
 *
 * @example:
 *      cge_event_publish(&WINDOWRESIZE_EVENT(1280, 720));
 */
void cge_event_publish(struct cge_event *event);

/**
 * @brief Dispatches all events on the event queue, to all registered listeners.
 *
 * @note Called every frame by cge_app.
 */
void cge_event_dispatch_all(void);

#endif // CGE_EVENTS_H
