#ifndef CGE_LOG_
#define CGE_LOG_

#include "time.h"

#define __FILENAME__ (__builtin_strrchr(__FILE__, '/') ? __builtin_strrchr(__FILE__, '/') + 1 : __FILE__)

#define CGE_LOG_INFO(msg, ...) _log(stdout, INFO, msg, ##__VA_ARGS__)
#define CGE_LOG_DEBUG(msg, ...) _log(stdout, DEBUG, msg, ##__VA_ARGS__)
#define CGE_LOG_ERROR(msg, ...) _log(stderr, ERROR, msg, ##__VA_ARGS__)

#define _log(fd, level, msg, ...)                                                                               \
        do {                                                                                                    \
                char date_time[20];                                                                             \
                struct tm *sTm;                                                                                 \
                time_t now = time(0);                                                                           \
                sTm = gmtime(&now);                                                                             \
                                                                                                                \
                strftime(date_time, sizeof(date_time), "%Y-%m-%d %H:%M:%S", sTm);                               \
                fprintf(fd, "[%s](%s) %s %s:%d " msg "\n", date_time, #level, __FILENAME__, __func__, __LINE__, \
                        ##__VA_ARGS__);                                                                         \
        } while (0);

#endif // CGE_LOG_
