#ifndef CGE_GUI_H
#define CGE_GUI_H

#include "cge/cge_layer.h"
#include <cge/cge_window.h>

void gui_init(void);
void gui_destroy(void);
void gui_update(f32 dt_seconds);

static const cge_layer_t gui_layer = { .on_attach = gui_init, .on_detach = gui_destroy, .on_update = gui_update };

#endif // CGE_GUI_H
