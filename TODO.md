# TODO
- [ ] Build docker in CI
- [ ] Multistage docker for faster build times
- [ ] Change unit test framework to gtest
- [ ] Report compiler warnings
- [ ] Generate doxygen



# DONE
- [X] Report test coverage
- [X] Build and report unit tests in gitlab
- [X] Build app from docker in gitlab ci
- [X] Build and report unit tests in gitlab
- [X] Implement cimgui
