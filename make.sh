#!/bin/bash

# Print usage information
usage()
{
    echo "Usage: $0 [options]"
    echo "Options:"
    echo "  -r, --release  Build in release mode"
    echo "  -d, --debug    Build in debug mode (default)"
    echo "  -c, --clean    Clean the build directory before building"
    echo "  -h, --help     Show this help message"
    exit 1
}

# Parse command line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -r|--release) BUILD_TYPE="Release"; shift ;;
        -d|--debug) BUILD_TYPE="Debug"; shift ;;
        -c|--clean) CLEAN=1; shift ;;
        -h|--help) usage; exit 0 ;;
        *) echo "Unknown option: $1"; usage; exit 1 ;;
    esac
done

# Set default build type if not specified
if [ -z ${BUILD_TYPE+x} ]
then
    BUILD_TYPE="Debug"
fi

# Set the number of parallel build jobs
NUM_JOBS=$(nproc)

# Clean the build directory (optional)
if [ -n "$CLEAN" ]
then
    rm -rf build
fi

# Create the build directory
mkdir -p build
cd build

# Configure the project with CMake
cmake .. -DCMAKE_BUILD_TYPE=$BUILD_TYPE

# Symbolic link of compile_commands.json to the root directory
ln -sf ./build/compile_commands.json ../compile_commands.json

# Build the project with make
make -j$NUM_JOBS
