#!/bin/bash

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
root_dir=$script_dir/..
build_dir=$root_dir/build

bash $script_dir/build.sh
cd $build_dir && make -j cge_coverage
bash $script_dir/prepare_test_results.sh
python $root_dir/test/vendor/Unity/auto/unity_to_junit.py $root_dir/tmp_test_report/
lcov_cobertura $root_dir/build/cge_coverage.total
