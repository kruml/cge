#!/bin/bash

# Get the root directory of the Git repository
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
root_dir=$script_dir/..

test_result_dir="${root_dir}/build/Testing/Temporary"
test_result_file="LastTest.log"
junit_parser_file="${root_dir}/test/vendor/Unity/auto/unity_to_junit.py"
test_report_dir="${root_dir}/tmp_test_report"

# Required file extension for unity_to_junit python scripts.
test_pass_ext="testpass"
test_fail_ext="testfail"

cd ${root_dir}
rm -rf ${test_report_dir} && mkdir ${test_report_dir}
cp ${test_result_dir}/${test_result_file} ${test_report_dir}
cd ${test_report_dir}

# Reset results files
> results.${test_pass_ext}
> results.${test_fail_ext}

# Find PASS and FAIL from Unity test results, saving them
# into their own files.
grep -e "FAIL" -e "IGNORE" ${test_result_file} >> results.${test_fail_ext}
grep "PASS" ${test_result_file} >> results.${test_pass_ext}
