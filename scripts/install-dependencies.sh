#!/bin/bash
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
root_dir=$script_dir/..
vcpkg_dir=$root_dir/vendor/vcpkg
vcpkg=$vcpkg_dir/vcpkg
triplet=x64-linux

if [ ! -f "$vcpkg" ]; then
    ls -la $vcpkg_dir
    bash $vcpkg_dir/bootstrap-vcpkg.sh
fi

#$vcpkg install --triplet=$triplet
