#!/bin/bash

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
root_dir=$script_dir/..
args=""

while getopts d flag
do
    case "${flag}" in
        d) args="$args --dry-run --Werror" ;;
    esac
done

files=$(find $root_dir/src $root_dir/include \( -name '*.h' -o -name '*.c' \))
echo checking files $files
clang-format -i ${files} $args --verbose
