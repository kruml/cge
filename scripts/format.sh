#!/bin/bash

run_docker=false
dry_run=false

# Parse options
while getopts ":dc" opt; do
    case ${opt} in
        d )
            run_docker=true
            ;;
        c )
            dry_run=true
            ;;
        \? )
            echo "Invalid option: $OPTARG" 1>&2
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

c_files=$(find src include \( -name '*.h' -o -name '*.c' \) -exec echo /opt/{} \;)
clang_args="-i ${c_files} --verbose"

if [ "$dry_run" = true ]; then
    clang_args="${clang_args} --Werror --dry-run"
fi

if [ "$run_docker" = true ]; then
    echo "Running with docker saschpe/clang-format"
    docker run \
        --rm \
        --privileged=true \
        --volume ${PWD}:/opt \
        saschpe/clang-format ${clang_args}
else
    clang-format -i ${clang_args}
fi

exit $?
