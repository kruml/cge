#!/bin/bash

root_dir=`git rev-parse --show-toplevel`
container_name="cge_build_container"
extra_args=''
detach=false

while getopts n:d flag
do
    case "${flag}" in
        n) container_name=${OPTARG} ;;
        d) detach=true
            extra_args="$extra_args --detach" ;;
    esac
done

if [ -n "$(docker ps -f "name=$container_name" -f "status=running" -q )" ]; then
    echo "$container_name is already running!"
    if [ "$detach" = true ]; then
        exit 0
    fi

    echo "attaching..."
    docker attach $container_name
fi

docker run --rm -it $extra_args --workdir $root_dir -v $root_dir:/$root_dir \
    --user "1000:1000" --name $container_name \
    cge-build:latest
#registry.gitlab.com/krum-game-engine/cge/cge-build:latest /bin/bash
