#!/bin/bash
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
docker_run_script=$script_dir/docker-run.sh
root_dir=$script_dir/..
build_dir=$root_dir/build
exec_with_docker=false
preset="debug"
clean=false
container_name=cge_build_container

while getopts dcr flag
do
    case "${flag}" in
        d) exec_with_docker=true ;;
        c) clean=true ;;
        r) preset="default" ;;
    esac
done

#rm -rf $build_dir && mkdir -p $build_dir
if [ "$clean" = true ]; then
    rm -rf $build_dir
fi

if [ "$exec_with_docker" = true ]; then
    echo "Building with docker!"
    $docker_run_script -d -n $container_name

    docker exec $container_name /bin/bash -c "mkdir -p $build_dir && \
        export CC=clang && \
        cmake -S $root_dir -B $build_dir --preset=$preset && \
        cmake --build $build_dir"
    exit 0
fi

mkdir -p $build_dir
cmake -S $root_dir -B $build_dir --preset=default
cmake --build $build_dir --parallel 4
