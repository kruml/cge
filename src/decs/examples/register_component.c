#include "ecs/decs.h"

typedef struct velocity_t {
        uint8_t x, y;
} Velocity;

DECS_COMPONENT_DECLARE(Velocity)

int main()
{
        DECS_COMPONENT_DEFINE(Velocity)
        Velocity vel = { .x = 1, .y = 2 };

        World *my_world = decs_world_create("Krummis world");
        EntityId player = decs_create_entity(my_world);

        DECS_COMPONENT_ADD(Velocity, my_world, vel, player)
}
