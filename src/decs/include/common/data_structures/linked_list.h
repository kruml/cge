#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "decs.h"
#include <stddef.h>

typedef struct Node Node;

struct Node {
        Node *next;
        void *data;
};

typedef struct {
        Node *head;
        size_t size;
} LinkedList;

LinkedList LinkedListCreate();
DecsStatus LinkedListAddNode(LinkedList *list, void *data, size_t size);
DecsStatus LinkedListRemoveNode(LinkedList *list, size_t idx);
DecsStatus LinkedListGetDataFromNode(LinkedList *list, size_t node, void *data, size_t size);
size_t LinkedListGetSize(LinkedList *list);

#endif // LINKED_LIST_H
