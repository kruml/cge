// TODO: Add header and comments

#ifndef SPARSE_SET_H
#define SPARSE_SET_H

#include "common/defines.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef DecsId SparseId;
typedef struct SparseSet SparseSet;

SparseSet *SparseSetCreate(size_t element_max, size_t element_size);
void SparseSetDestroy(SparseSet *ss);
void SparseSetAddElement(SparseSet *ss, SparseId id, void *element, size_t size);
void *SparseSetGetElement(SparseSet *ss, SparseId id, size_t element_size);
void SparseSetRemoveElement(SparseSet *ss, SparseId id, size_t size);

#endif // SPARSE_SET_H
