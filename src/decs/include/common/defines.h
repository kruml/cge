#ifndef DEFINES_H_
#define DEFINES_H_

#define INLINE inline __attribute__((always_inline))
#define STATIC_INLINE static inline __attribute__((always_inline))

#define IS_BIT_SET(number, bit)             \
        do {                                \
                ((n & (1 << (k - 1))) == 1) \
        } while (0);

#endif // DEFINES_H_
