/**
 * @file decs.h
 *
 * @brief Interface for everything DECS.
 *
 * This file should be the only include necessary to interface with DECS.
 * Create worlds, add entities, manage components and systems etc...
 */

#ifndef DECS_H
#define DECS_H

/* --------------------- Imports --------------------- */
#include <stddef.h>
#include <stdint.h>

/* --------------------- Local Imports --------------------- */
#include "common/defines.h"
#include "ecs/entity.h"
#include "ecs/types.h"

/* --------------------- Defines --------------------- */
#define DECS_CAST(T, V) ((T)(V))

/* --------------------- Types --------------------- */

/**
 * Status type used by DECS.
 */
typedef enum { kDecsSuccess = 0, kDecsFail } DecsStatus;

/**
 * Definition for system callback function
 */
typedef DecsStatus (*SystemUpdateCallback)(float);

/**
 * Opaque world type, container for entities, components and systems.
 */
typedef struct World World;

/**
 * Opaque system type
 */
typedef struct System System;

/**
 * Entity iterator iterates entities (duh)
 * Can be used by systems to safely iterate all entities in a world.
 * As of now, the iterator only points to ALL entities in a world, and cannot
 * for instance iterate a filtered query.
 */
typedef Entity *EntityIterator;

/* --------------------- Update functions --------------------- */

/**
 * When calling the update function, all systems will be run.
 *
 * @param   world - World to update
 * @oaram   dt    - Delta time since last update
 * @return  kDecsSuccess if everything went well, else kDecsFail.
 */
DecsStatus decs_Update(World *world, float dt);

// TODO: Docs
DecsStatus decs_Draw(World *world, float dt);

/* --------------------- World functions --------------------- */

/**
 * Creates and initiates an opaque pointer to a World.
 *
 * @param   name - Name of world
 *
 * @return  pointer to new world
 */
World *decs_WorldCreate(const char *name);

/**
 * Destroy world and all its components, entities and systems.
 *
 * @param   world - world to destroy
 *
 * @return  Success or fail status
 */
DecsStatus decs_WorldDestroy(World *world);

char *decs_WorldGetName(World *world);

/* --------------------- Entity functions --------------------- */

/**
 * Create an entity and add it to the world.
 *
 * @param   world - world to put new entity in.
 *
 * @return  ID for new entity
 */
EntityId decs_CreateEntity(World *world);

/**
 * Removes an entity from a world.
 *
 * @param   world - world to remove entity from
 * @param   entity - entity to remove
 */
void decs_RemoveEntity(World *world, EntityId entity);

/**
 * Returns iterator, pointing to first entity in a world.
 *
 * @param   world - world which entities should be iterated
 *
 * @return  iterator to first entity.
 */
EntityIterator decs_EntityIteratorBegin(World *world);

/**
 * Returns iterator, pointing to last entity in a world.
 *
 * @param   world - world which entities should be iterated
 *
 * @return  iterator to last entity,
 */
EntityIterator decs_EntityIteratorEnd(World *world);

/**
 * Returns iterator, pointing to the next entity in the iterator.
 *
 * @param   it   - iterator to fetch next entity from
 *
 * @return  iterator to next entity
 */
EntityIterator decs_EntityIteratorNext(EntityIterator it);

/* --------------------- Component functions --------------------- */

/**
 * @brief Should only be used through COMPONENT macros!
 */
ComponentId decs_ComponentInit();

/**
 * @brief Should only be used through COMPONENT macros!
 */
void decs_ComponentAdd(World *world, EntityId entity, ComponentId component_id, void *component, size_t component_size);

/**
 * @brief Should only be used through COMPONENT macros!
 */
void decs_ComponentDestroy(World *world, EntityId entity, ComponentId component_id, size_t element_size);

/**
 * @brief Should only be used through COMPONENT macros!
 */
void *decs_ComponentGet(World *world, EntityId entity, ComponentId component_id, size_t element_size);

#define COMPONENT_SIZE_VAR(T) __##T##_size
#define COMPONENT_SET_VAR(T) __##T##_set
#define COMPONENT_ID_VAR(T) __##T##_id

/**
 * @brief Used for compile time calculation of user define components
 */
#define DECS_COMPONENT_SIZE(T) __##T##_size

/**
 * @brief Compile time created id for component
 */
#define DECS_COMPONENT_ID(T) __##T##_id

/**
 * @def  DECS_COMPONENT_DECLARE(T)
 * @brief Declare size and id variable for user defined component
 *
 * Declare a user defined component, and created variables to hold compile time
 * calculated size and component id. These variables will be used with the
 * component functions, so the user does not have to do costly sizeof(), or
 * handle comonent size themselves.
 *
 * @example register_component.c
 */
#define DECS_COMPONENT_DECLARE(T)         \
        ComponentId DECS_COMPONENT_ID(T); \
        size_t DECS_COMPONENT_SIZE(T);

/**
 * @def  DECS_COMPONENT_DEFINE(T)
 * @brief Define size and id variable for user defined component
 *
 * Calculate size and get component id at compile time. These variables will be
 * used with the component functions, so the user does not have to do costly
 * sizeof(), or handle comonent size themselves.
 *
 * @example register_component.c
 */
#define DECS_COMPONENT_DEFINE(T)            \
        DECS_COMPONENT_SIZE(T) = sizeof(T); \
        DECS_COMPONENT_ID(T) = decs_ComponentInit();

/**
 * @def  DECS_COMPONENT_ADD(T)
 * @brief Wrapper macro for decs_component_add()
 *
 * Use pre-calculated component size and id with decs_component_add()
 *
 * @example register_component.c
 */
#define DECS_COMPONENT_ADD(T, world, component, entity_id)                                                     \
        do {                                                                                                   \
                decs_ComponentAdd(world, entity_id, DECS_COMPONENT_ID(T), &component, DECS_COMPONENT_SIZE(T)); \
        } while (0);

/**
 * @def  DECS_COMPONENT_GET(T)
 * @brief Wrapper macro for decs_component_get()
 *
 * Use pre-calculated component size and id with decs_component_get()
 *
 * @example register_component.c
 */
#define DECS_COMPONENT_GET(T, world, entity) \
        DECS_CAST(T *, decs_ComponentGet(world, entity, DECS_COMPONENT_ID(T), DECS_COMPONENT_SIZE(T)));

/**
 * @def  DECS_COMPONENT_DESTROY(T)
 * @brief Wrapper macro for decs_component_destroy()
 *
 * Use pre-calculated component size and id with decs_component_destroy()
 *
 * @example register_component.c
 */
#define DECS_COMPONENT_DESTROY(T, world, entity)                                                    \
        do {                                                                                        \
                decs_ComponentDestroy(world, entity, DECS_COMPONENT_ID(T), DECS_COMPONENT_SIZE(T)); \
        } while (0);

/* --------------------- System functions --------------------- */
System *decs_SystemCreate();
void decs_SystemRegisterCallback(SystemUpdateCallback cb);
DecsStatus decs_SystemUpdate(World *world, float dt);
SystemId decs_SystemRegister(World *world, SystemUpdateCallback cb, Signature signature);

#endif // DECS_H
