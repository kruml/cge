#include "state_manager.h"

#include <stdlib.h>

static const int STARTING_CAPACITY = 3;

int statemanagerInit(StateManager *manager)
{
        manager->capacity = STARTING_CAPACITY;
        manager->top = -1;
        manager->stack = malloc(manager->capacity * sizeof(State *));
        return 1;
}

int statemanagerFree(StateManager *manager)
{
        while (manager->top > -1) {
                statemanagerPop(manager);
        }
        free(manager->stack);
        return 1;
}

int statemanagerInternalScale(StateManager *manager)
{
        manager->capacity *= 2;
        manager->stack = realloc(manager->stack, manager->capacity * sizeof(State *));
        return manager->capacity;
}

int statemanagerPop(StateManager *manager)
{
        if (manager->top < 0) {
                return 0;
        }
        State *top_state = statemanagerTop(manager);
        if (top_state->destroy_world != NULL) {
                top_state->destroy_world(top_state->world);
        }
        return --manager->top;
}

int statemanagerPush(StateManager *manager, State *state)
{
        if (manager->top + 1 >= manager->capacity) {
                statemanagerInternalScale(manager);
        }
        manager->top++;
        manager->stack[manager->top] = state;
        return 1;
}

State *statemanagerTop(StateManager *manager)
{
        return manager->stack[manager->top];
}

int statemanagerUpdate(StateManager *manager, float dt)
{
        State *top_state = statemanagerTop(manager);
        if (top_state->update_world != NULL) {
                top_state->update_world(top_state->world, dt);
                return 1;
        }
        return 0;
}

int statemanagerDraw(StateManager *manager, float dt)
{
        State *top_state = statemanagerTop(manager);
        if (top_state->draw_world != NULL) {
                top_state->draw_world(top_state->world, dt);
                return 1;
        }
        return 0;
}
