#include "ecs/entity.h"
#include "common/log.h"
#include "data_structures/stack.h"
#include "ecs/types.h"

#include <assert.h>

STACK_DECLARE(stack_int, EntityId)
STACK_DEFINE(stack_int, EntityId)

// +1 since we dont use 0 as entity index
#define STARTING_ENTITIES_BUF (1000000 + 1)

EntityId EntityCreate(EntityList *entity_list)
{
        EntityId new_entity = (EntityId)stack_int_pop(entity_list->available_ids);
        entity_list->active++;
        entity_list->buf[new_entity].id = new_entity;
        entity_list->buf[new_entity].signature = 0;
        return new_entity;
}

void EntitySetSignature(EntityList *entity_list, EntityId entity, Signature signature)
{
        entity_list->buf[entity].signature = signature;
}

Signature EntityGetSignature(EntityList *entity_list, EntityId entity)
{
        return entity_list->buf[entity].signature;
}

void EntityDestroy(EntityList *entity_list, EntityId id)
{
        LOG_DEBUG("Remove entity %lu\n", id);

        entity_list->buf[id].id = -1;
        // TODO: define signature empty;
        entity_list->buf[id].signature = 0;
        entity_list->active--;
        stack_int_push(entity_list->available_ids, id);
}

void EntityListCreate(EntityList *list)
{
        LOG_DEBUG("Creating entity list!\n");
        /* EntityList *list = malloc(sizeof(EntityList)); */
        list->buf = malloc(sizeof(Entity) * STARTING_ENTITIES_BUF);
        assert(list->buf != NULL);

        list->max = STARTING_ENTITIES_BUF;
        list->active = 0;

        list->available_ids = stack_int_create();
        bool push_res = false;
        for (int i = STARTING_ENTITIES_BUF; i > 0; i--) {
                push_res = stack_int_push(list->available_ids, i);
                assert(push_res);
        }
}

void EntityListDestroy(EntityList *entities)
{
        LOG_DEBUG("Destroying entity list!\n");
        stack_int_destroy(entities->available_ids);
        free(entities->buf);
}
