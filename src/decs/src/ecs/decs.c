#include "ecs/decs.h"
#include "ecs/component.h"
#include "ecs/entity.h"
#include "ecs/system.h"
#include "ecs/types.h"

#include "common/log.h"
#include "ecs/data_structures/stack.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define COMPONENTS_ARRAY_STARTING_SIZE (100)

static ComponentId cid = 0;
/* World */

struct World {
        EntityList entities;
        ComponentList components;
        SystemList systems;
        char *name;
};

World *decs_WorldCreate(const char *name)
{
        LOG_DEBUG("Creating world %s\n", name);

        World *world = malloc(sizeof(World));
        world->name = strdup(name);
        assert(world->name != NULL);

        EntityListCreate(&world->entities);
        /* world->components = component_list_create(); */
        ComponentListCreate(&world->components);
        system_list_create(&world->systems);

        return world;
}

DecsStatus decs_WorldDestroy(World *world)
{
        EntityListDestroy(&world->entities);
        ComponentListDestroy(&world->components);
        system_list_destroy(&world->systems);
        free(world->name);
        free(world);
        return 1;
}

char *decs_WorldGetName(World *world)
{
        assert(world != NULL);
        return world->name;
}

DecsStatus decs_Draw(World *world, float dt)
{
        return 1;
}

DecsStatus decs_Update(World *world, float dt)
{
        return system_list_update(&world->systems, dt);
}

EntityId decs_CreateEntity(World *world)
{
        return EntityCreate(&world->entities);
}

void decs_RemoveEntity(World *world, EntityId entity)
{
        EntityDestroy(&world->entities, entity);
}

EntityIterator decs_EntityIteratorBegin(World *world)
{
        return world->entities.buf;
}

EntityIterator decs_EntityIteratorEnd(World *world)
{
        return &world->entities.buf[world->entities.active];
}
EntityIterator decs_EntityIteratorNext(EntityIterator it)
{
        return ++it;
}

void decs_ComponentAdd(World *world, EntityId entity, ComponentId component_id, void *component, size_t component_size)
{
        ComponentAdd(entity, &world->components, component_id, component, component_size);
        Signature new_signature = (component_id | EntityGetSignature(&world->entities, entity));
        EntitySetSignature(&world->entities, entity, new_signature);
}

void decs_ComponentDestroy(World *world, EntityId entity, ComponentId component, size_t element_size)
{
        ComponentDestroy(entity, &world->components, component, element_size);
}

void *decs_ComponentGet(World *world, EntityId entity, ComponentId component_id, size_t element_size)
{
        return ComponentGet(entity, &world->components, component_id, element_size);
}

// TODO: This weird gloablly shared component initiatior nees to be reworked.
ComponentId decs_ComponentInit()
{
        assert(cid < 64);
        cid++;
        return (1 << cid);
}

SystemId decs_SystemRegister(World *world, SystemUpdateCallback cb, Signature signature)
{
        System *new_system = system_create(cb, signature);
        return system_add(&world->systems, new_system);
}
