#include "ecs/component.h"

#include "common/data_structures/sparse_set.h"
#include "common/log.h"

#include <assert.h>
#include <memory.h>
#include <stdlib.h>

#define ELEMENT_OFFSET(first_element, offset, element_size) \
        (void *)((((char *)first_element)) + (((size_t)offset) * ((size_t)element_size)))

typedef SparseId *p_sparse_array_t;
typedef SparseId *p_dense_array_t;

struct SparseSet {
        SparseId size;
        SparseId max;
        p_sparse_array_t sparse;
        p_dense_array_t dense;
        void *element;
};

SparseSet *SparseSetCreate(size_t element_max, size_t element_size)
{
        SparseSet *set = malloc(sizeof(SparseSet));
        set->max = element_max;
        set->size = 0;
        set->sparse = malloc(sizeof(SparseId) * element_max);
        set->dense = malloc(sizeof(SparseId) * element_max);
        set->element = malloc(element_size * element_max);
        return set;
}

void SparseSetDestroy(SparseSet *ss)
{
        free(ss->dense);
        free(ss->sparse);
        free(ss->element);
        free(ss);
}

bool _doesElementExist(SparseSet *ss, SparseId id)
{
        if (id > ss->max) {
                return false;
        }
        return ss->dense[ss->sparse[id]] == id;
}

void _scale(SparseSet **ss, size_t factor, size_t elem_size)
{
        SparseSet *ssptr = *ss;
        ssptr->max *= factor;
        ssptr = realloc(ssptr, sizeof(SparseSet) + (ssptr->max * elem_size));
        ssptr->sparse = realloc(ssptr->sparse, sizeof(SparseId) * ssptr->max);
        ssptr->dense = realloc(ssptr->dense, sizeof(SparseId) * ssptr->max);
}

INLINE
void SparseSetAddElement(SparseSet *ss, SparseId id, void *new_element, size_t element_size)
{
        assert(new_element != NULL);

        /* Check if already exists */
        if (_doesElementExist(ss, id)) {
                // TODO: Log
                return;
        }

        /* Scale sparse array if needed */
        if (ss->max < id)
                _scale(&ss, 2, element_size);

        /* Add */
        ss->sparse[id] = ss->size;
        ss->dense[ss->size] = id;
        void *ss_element = ELEMENT_OFFSET(ss->element, ss->size, element_size);
        assert(ss_element != NULL);
        memcpy(ss_element, new_element, element_size);
        ss->size++;

        assert(_doesElementExist(ss, id));
}

void SparseSetRemoveElement(SparseSet *ss, SparseId id, size_t size)
{
        if (!(_doesElementExist(ss, id))) {
                return;
        }
        LOG_DEBUG("Remove element id %d at idx %d\n", id, ss->sparse[id]);
        void *remove_element = ELEMENT_OFFSET(ss->element, ss->sparse[id], size);

        LOG_DEBUG("Replace with element id %d at idx %d\n", ss->dense[ss->size - 1],
                  ss->sparse[ss->dense[ss->size - 1]]);
        void *last_element = ELEMENT_OFFSET(ss->element, ss->sparse[ss->dense[ss->size - 1]], size);
        memcpy(remove_element, last_element, size);

        /* Change places of id with last element in dense */
        ss->dense[ss->sparse[id]] = ss->dense[ss->size - 1];
        ss->sparse[ss->size - 1] = ss->sparse[id];
        ss->size--;

        assert(!(_doesElementExist(ss, id)));
}

void *SparseSetGetElement(SparseSet *ss, SparseId id, size_t element_size)
{
        void *element = NULL;
        if (_doesElementExist(ss, id)) {
                size_t idx = ss->sparse[id];
                element = ELEMENT_OFFSET(ss->element, idx, element_size);
        }
        return element;
}
