
#include "ecs/decs.h"

#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#define SS_START_SIZE (32)

static World *my_world;

typedef struct vector_t {
        uint32_t x, y;
} Vector;

typedef struct velocity_t {
        uint8_t x, y;
} Velocity;

DECS_COMPONENT_DECLARE(Vector)
DECS_COMPONENT_DECLARE(Velocity)

DecsStatus system_cb_velocity(float dt)
{
        /* LOG_DEBUG("CALLBACK FUNCTION 1 TEST %f!\n", dt); */
        Signature system_signature_velocity = (DECS_COMPONENT_ID(Vector) | DECS_COMPONENT_ID(Velocity));

        Velocity *vel;
        Vector *vec;
        EntityIterator it = decs_EntityIteratorBegin(my_world);
        EntityIterator end = decs_EntityIteratorEnd(my_world);

        for (; it < end; it = decs_EntityIteratorNext(it)) {
                if ((system_signature_velocity & it->signature) == system_signature_velocity) {
                        vel = DECS_COMPONENT_GET(Velocity, my_world, it->id);
                        vec = DECS_COMPONENT_GET(Vector, my_world, it->id);
                        assert(vel != NULL);
                        assert(vel != NULL);

                        vec->x += vel->x;
                        vec->y += vel->y;
                        /* LOG_DEBUG("New velocity entity %lu - x %u - y %u\n", it->id, vel->x, */
                        /* vel->y) */
                }
        }
        return kDecsSuccess;
}

DecsStatus system_cb_movement(float dt)
{
        /* LOG_DEBUG("CALLBACK FUNCTION 2 TEST! %f\n", dt); */
        /* vector_t *pos; */
        /* velocity_t *vel; */

        /* EntityIterator it = kecs_entity_iterator_begin(my_world); */
        /* EntityIterator end = kecs_entity_iterator_end(my_world); */

        /* for (; it < end; it = kecs_entity_iterator_next(it)) { */
        /*   if ((it->signature) > 0) { */
        /*     ECS_COMPONENT_GET(velocity_t, vel, my_world, it->id) */
        /*     ECS_COMPONENT_GET(vector_t, pos, my_world, it->id) */

        /*     assert(vel != NULL); */
        /*     assert(pos != NULL); */

        /*     pos->x += vel->x; */
        /*     pos->y += vel->y; */
        /*     LOG_DEBUG("New position entity %lu - x %u - y %u", it->id, pos->x,
   * pos->y) */
        /*   } */
        /* } */
        return kDecsSuccess;
}

int main()
{
        DECS_COMPONENT_DEFINE(Vector)
        DECS_COMPONENT_DEFINE(Velocity)

        Vector vect1 = { .x = 1, .y = 2 };
        Vector vect2 = { .x = 3, .y = 4 };
        Vector vect3 = { .x = 5, .y = 6 };

        Velocity fast = { .x = 255, .y = 125 };
        Velocity slow = { .x = 10, .y = 5 };

        my_world = decs_WorldCreate("Krummis world");
        EntityId player = decs_CreateEntity(my_world);
        EntityId orc1 = decs_CreateEntity(my_world);
        EntityId orc2 = decs_CreateEntity(my_world);

        DECS_COMPONENT_ADD(Vector, my_world, vect1, player)
        DECS_COMPONENT_ADD(Vector, my_world, vect2, orc1)
        DECS_COMPONENT_ADD(Vector, my_world, vect3, orc2)
        DECS_COMPONENT_ADD(Velocity, my_world, fast, player)
        DECS_COMPONENT_ADD(Velocity, my_world, slow, orc2)

        decs_SystemRegister(my_world, &system_cb_velocity, 1);
        decs_SystemRegister(my_world, &system_cb_movement, 2);

        for (int i = 0; i < 5; i++) {
                decs_Update(my_world, 1.0);
                // decs_update(my_world, 1.0);
                sleep(1);
        }

        decs_WorldDestroy(my_world);
}
