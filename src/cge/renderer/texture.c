#include "cge/cge_texture.h"
#include "utils.h"

#include <glad/glad.h>
#include <stb_image.h>

struct cge_texture2d cge_texture_create(const char *path)
{
        struct cge_texture2d self = {
                .path = cge_assetpath(path),
        };

        int channels;
        stbi_uc *data = stbi_load(self.path, &self.width, &self.height, &channels, 0);

        glCreateTextures(GL_TEXTURE_2D, 1, &self.renderer_id);
        glTextureStorage2D(self.renderer_id, 1, GL_RGB8, self.width, self.height);
        glTextureParameteri(self.renderer_id, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTextureParameteri(self.renderer_id, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTextureSubImage2D(self.renderer_id, 0, 0, 0, self.width, self.height, GL_RGB, GL_UNSIGNED_BYTE, data);

        stbi_image_free(data);

        return self;
}

void cge_texture_destroy(struct cge_texture2d *self)
{
        glDeleteTextures(1, &self->renderer_id);
}

void cge_texture_bind(const struct cge_texture2d *self, u32 slot)
{
        glBindTextureUnit(slot, self->renderer_id);
}
