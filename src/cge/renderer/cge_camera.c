#include "cge/cge_camera.h"

#include "cge_assert.h"
#include "cglm/affine-mat.h"
#include "cglm/affine-pre.h"
#include "cglm/cam.h"
#include <cglm/cglm.h>

#include <pthread.h>
#include <string.h>

static void calculate_viewprojection(struct cge_camortho *self)
{
        glm_mat4_mul(self->view_matrix, self->projection_matrix, self->view_projection_matrix);
}

static void calculate_projection(struct cge_camortho *self, f32 width, f32 height)
{
        f32 aspect_ratio = width / height;
        f32 right = self->orthosize * aspect_ratio;
        f32 left = -right;
        f32 top = self->orthosize;
        f32 bottom = -top;
        glm_ortho(left, right, bottom, top, -1.0F, 1.0F, self->projection_matrix);
}

void cge_camortho_update_view_transform(struct cge_camortho *self)
{
        mat4 transform = GLM_MAT4_IDENTITY_INIT;

        glm_translate(transform, self->position);
        glm_rotate(transform, glm_rad(self->rotation), (vec3){ 0, 0, 1 });
        glm_inv_tr(transform);
        memcpy(self->view_matrix, transform, sizeof(transform));
        calculate_viewprojection(self);
}

struct cge_camortho cge_camortho_init(f32 width, f32 height)
{
        struct cge_camortho self = { .view_matrix = GLM_MAT4_IDENTITY_INIT, .orthosize = 5.0F };
        calculate_projection(&self, width, height);
        cge_camortho_update_view_transform(&self);
        return self;
}

void cge_camortho_update_projection(struct cge_camortho *self, f32 width, f32 height)
{
        CGE_ASSERT_NOT_NULL(self, "");
        calculate_projection(self, width, height);
        calculate_viewprojection(self);
#if 1
        CGE_LOG_DEBUG("View matrix:");
        for (int i = 0; i < 4; i++) {
                printf("%f %f %f %f\n", self->view_projection_matrix[i][0], self->view_projection_matrix[i][1],
                       self->view_projection_matrix[i][2], self->view_projection_matrix[i][3]);
        }
#endif
}
