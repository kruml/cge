#include "cge/cge_renderer.h"
#include "cge/cge_buffer.h"
#include "cge/cge_camera.h"
#include "cge/cge_log.h"
#include "cge/cge_shader.h"
#include "cge/cge_texture.h"

#include <GL/gl.h>
#include <glad/glad.h>
#include <cglm/cglm.h>

struct scene_data {
        mat4 view_projection_matrix;
};

struct renderer_data {
        cge_varray_t *varray;
        cge_shader *flatcolor_shader;
        cge_shader *texture_shader;
};

static struct renderer_data _data = { 0 };
//static cge_varray_t *vertex_array = NULL;
//static cge_shader *flatcolor_shader = NULL;

static const vec4 clear_color = { 0.1F, 0.1F, 0.1F, 1 };

struct scene_data renderer_data = { 0 };

static void cge_renderer_submit(const cge_varray_t *vertex_array)
{
        cge_varray_bind(vertex_array);
        u32 indexbuf_count = cge_ibuf_count(cge_varray_get_ibuf(vertex_array));
        glDrawElements(GL_TRIANGLES, indexbuf_count, GL_UNSIGNED_INT, NULL);
}

void cge_renderer_init(void)
{
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_DEPTH_TEST);
        _data.flatcolor_shader = cge_shader_create_from_file("/shaders/flat_color.glsl");
        _data.texture_shader = cge_shader_create_from_file("/shaders/texture.glsl");
        cge_shader_bind(_data.texture_shader);
        cge_shader_set_int(_data.texture_shader, "u_Texture", 0);

        /* clang-format off */
        float square_vertices[5 * 4] = {
                -0.5F, -0.5F, 0.0F, 0.0F, 0.0F,
                 0.5F, -0.5F, 0.0F, 1.0F, 0.0F,
                 0.5F,  0.5F, 0.0F, 1.0F, 1.0F,
                -0.5F,  0.5F, 0.0F, 0.0F, 1.0F,
        };
        /* clang-format on */

        cge_vbuf_t *vertex_buf = cge_vbuf_new(square_vertices, sizeof(square_vertices));
        cge_vbuf_bind(vertex_buf);
        {
                cge_bufferelement_t elements[] = {
                        cge_bufferelement_create(CGE_SHADER_DATA_VEC3, "a_Position"),
                        cge_bufferelement_create(CGE_SHADER_DATA_VEC2, "a_TexCoord"),
                };

                cge_vbuf_set_layout(vertex_buf, cge_bufferlayout_new(elements, countof(elements)));
        }

        unsigned int indices[] = { 0, 1, 2, 2, 3, 0 };
        cge_ibuf_t *index_buf = cge_ibuf_new(indices, countof(indices));
        cge_ibuf_bind(index_buf);

        _data.varray = cge_varray_new();
        cge_varray_add_vbuf(_data.varray, vertex_buf);
        cge_varray_set_ibuf(_data.varray, index_buf);
}

void cge_renderer_deinit(void)
{
        cge_varray_destroy(_data.varray);
        cge_shader_destroy(_data.flatcolor_shader);
}

void cge_renderer_begin_scene(const struct cge_camortho *camera)
{
        memcpy(renderer_data.view_projection_matrix, camera->view_projection_matrix,
               sizeof(renderer_data.view_projection_matrix));
        cge_renderer_set_clear_color(clear_color);
        cge_renderer_clear();

        cge_shader_bind(_data.flatcolor_shader);
        cge_shader_set_mat4(_data.flatcolor_shader, "u_ViewProjection", renderer_data.view_projection_matrix);

        cge_shader_bind(_data.texture_shader);
        cge_shader_set_mat4(_data.texture_shader, "u_ViewProjection", renderer_data.view_projection_matrix);
}

void cge_renderer_end_scene(void)
{
}

void cge_renderer_draw_quad2d(vec2 position, vec2 size, vec4 color)
{
        cge_renderer_draw_quad3d((vec3){ position[0], position[1], 0.0f }, size, color);
}

void cge_renderer_draw_quad3d(vec3 position, vec2 size, vec4 color)
{
        mat4 transform = GLM_MAT4_IDENTITY_INIT;
        glm_translate(transform, position);
        glm_scale(transform, (vec3){ size[0], size[1], size[2] });

        cge_shader_bind(_data.flatcolor_shader);
        cge_shader_set_mat4(_data.flatcolor_shader, "u_Transform", transform);
        cge_shader_set_vec4(_data.flatcolor_shader, "u_Color", color);
        cge_renderer_submit(_data.varray);
}

void cge_renderer_draw_quad2d_tex(vec2 position, vec2 size, const struct cge_texture2d *texture)
{
        cge_renderer_draw_quad3d_tex((vec3){ position[0], position[1], 0.0f }, size, texture);
}

void cge_renderer_draw_quad3d_tex(vec3 position, vec2 size, const struct cge_texture2d *texture)
{
        mat4 transform = GLM_MAT4_IDENTITY_INIT;
        glm_translate(transform, position);
        glm_scale(transform, (vec3){ size[0], size[1], size[2] });

        cge_shader_bind(_data.texture_shader);
        cge_shader_set_mat4(_data.texture_shader, "u_Transform", transform);
        cge_texture_bind(texture, 0);
        cge_renderer_submit(_data.varray);
}

void cge_renderer_set_clear_color(const vec4 color)
{
        glClearColor(color[0], color[1], color[2], color[3]);
}

void cge_renderer_clear(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void cge_renderer_on_winresize(u32 width, u32 height)
{
        glViewport(0, 0, width, height);
}
