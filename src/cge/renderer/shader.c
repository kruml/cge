#include "cge_assert.h"
#include "cge_core.h"
#include "cge_log.h"
#include "cge_shader.h"
#include "cge_status.h"

#include "utils.h"

#include <ctype.h>
#include <glad/glad.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stb_ds.h>

#define SHADERFILE_TYPE_TOKEN "#type"
#define SHADERSTR_MAXLEN (8) // fragment

struct cge_shader {
        u32 id;
};

enum shader_type { VERTEX_SHADER = 1, FRAGMENT_SHADER = 2, NO_SHADER };

struct shader_source {
        enum shader_type type;
        char *source;
};

static enum shader_type parse_shader_type(const char *type_str)
{
        if (strcmp(type_str, "vertex") == 0)
                return VERTEX_SHADER;
        if (strcmp(type_str, "fragment") == 0)
                return FRAGMENT_SHADER;

        CGE_ASSERT(false, "Unknown shadertype");
        return NO_SHADER;
}

static GLuint shader_type_to_opengl(enum shader_type type)
{
        if (type == VERTEX_SHADER)
                return GL_VERTEX_SHADER;
        if (type == FRAGMENT_SHADER)
                return GL_FRAGMENT_SHADER;

        CGE_ASSERT(false, "Unknown shadertype");
        return 0;
}

/**
 * Returns size of file, and points to beginning of file
 *
 * @param file
 * @return
 */
static inline usize filesize(FILE *file)
{
        fseek(file, 0, SEEK_END);
        u64 size = ftell(file);
        fseek(file, 0, SEEK_SET);
        return size;
}

static struct shader_source *parse_shaders(const char *buffer, usize len)
{
        struct shader_source *sources = NULL;
        const char *bufptr = buffer;
        const char *bufend = buffer + len;
        do {
                /* Parse shader type */
                bufptr = strstr(bufptr, SHADERFILE_TYPE_TOKEN);
                if (bufptr == NULL) {
                        CGE_LOG_ERROR("Error parsing shader file. Could not find %s", SHADERFILE_TYPE_TOKEN);
                        break;
                }

                usize len = strlen(SHADERFILE_TYPE_TOKEN);
                if (!advance_ptr(bufptr, len, bufend))
                        break;

                // handles spaces between #type and typestring
                while (isblank((*bufptr))) {
                        if (!advance_ptr(bufptr, 1, bufend))
                                break;
                }

                char *eol = strchr(bufptr, '\n');
                if (eol == NULL) {
                        CGE_LOG_ERROR("Error parsing shader file");
                        break;
                }

                u8 typestrlen = (eol - bufptr);
                char typestr[typestrlen + 1];
                strncpy(typestr, bufptr, SHADERSTR_MAXLEN);
                typestr[typestrlen] = '\0';

                if (!advance_ptr(bufptr, (typestrlen + 1), bufend))
                        break;

                // parse shader code
                const char *next_shaderstart = strstr(bufptr, SHADERFILE_TYPE_TOKEN);
                if (next_shaderstart == NULL) {
                        next_shaderstart = bufend;
                }

                usize shader_size = next_shaderstart - bufptr;
                struct shader_source shader = { .type = parse_shader_type(typestr),
                                                .source = cge_malloc(char, (shader_size + 1)) };

                strncpy(shader.source, bufptr, shader_size);
                shader.source[shader_size] = '\0';

                arrput(sources, shader);
                CGE_LOG_DEBUG("Parsed shader type %s", typestr);
                bufptr = next_shaderstart;
        } while ((bufptr < bufend));

        return sources;
}

static cge_status_t create_shader(GLuint program, const struct shader_source *sources, size_t count)
{
        // Create an empty shader handle
        GLuint *created_shaders = NULL;
        for (u8 i = 0; i < count; i++) {
                GLuint shader = glCreateShader(shader_type_to_opengl(sources[i].type));
                arrput(created_shaders, shader);

                // Send the shader source code to GL
                const GLchar *source = (const GLchar *)sources[i].source;
                glShaderSource(shader, 1, &source, 0);

                // Compile the shader
                glCompileShader(shader);

                GLint isCompiled = 0;
                glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
                if (isCompiled == GL_FALSE) {
                        GLint maxLength = 0;
                        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

                        // The maxLength includes the NULL character
                        GLchar info_log[maxLength];
                        glGetShaderInfoLog(shader, maxLength, &maxLength, info_log);

                        // We don't need the shader anymore.
                        glDeleteShader(shader);

                        arrfree(created_shaders);

                        // Use the infoLog as you see fit.
                        CGE_LOG_ERROR("Shader compilation error: %s", info_log);
                        CGE_ASSERT(false, "Shader compilation failed!");
                        return CGE_FAIL;
                }

                // Attach our shader to our program
                glAttachShader(program, shader);
        }

        // Link our program
        glLinkProgram(program);

        // Note the different functions here: glGetProgram* instead of glGetShader*.
        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
        if (isLinked == GL_FALSE) {
                GLint maxLength = 0;
                glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

                // The maxLength includes the NULL character
                GLchar info_log[maxLength];
                glGetProgramInfoLog(program, maxLength, &maxLength, info_log);

                // We don't need the program anymore.
                glDeleteProgram(program);
                // Don't leak shaders either.
                for (int i = 0; i < arrlen(created_shaders); i++)
                        glDeleteShader(created_shaders[i]);

                arrfree(created_shaders);
                // Use the infoLog as you see fit.

                CGE_LOG_ERROR("Shader link error: %s", info_log);
                CGE_ASSERT(false, "Shader linking failed!");
                return CGE_FAIL;
        }

        // Always detach shaders after a successful link.
        for (int i = 0; i < arrlen(created_shaders); i++)
                glDetachShader(program, created_shaders[i]);

        arrfree(created_shaders);

        return CGE_OK;
}

static char *readfile(const char *path)
{
        FILE *file = fopen(path, "r");
        if (!file) {
                CGE_LOG_ERROR("Invalid file %s", path);
                return NULL;
        }

        CGE_LOG_DEBUG("Reading shaderfile %s", path);

        usize size = filesize(file);
        char *buffer = malloc(sizeof(*buffer) * size + 1);
        fread(buffer, 1, size, file);
        buffer[size] = '\0';
        fclose(file);

        return buffer;
}

cge_shader *cge_shader_create_from_file(const char *filepath)
{
        cge_shader *result = NULL;
        char *filepath_abs = cge_assetpath(filepath);
        cge_assetpath("asdasd");
        char *shaderbuf = readfile(filepath_abs);
        cge_free(filepath_abs);

        struct shader_source *sources = parse_shaders(shaderbuf, strlen(shaderbuf));
        if (!sources) {
                CGE_LOG_DEBUG("No shaders succesfully parsed!");
                goto cleanup;
        }

        GLuint program = glCreateProgram();
        if (create_shader(program, sources, arrlen(sources)) != CGE_OK) {
                goto cleanup;
        }

        result = malloc(sizeof(cge_shader));
        result->id = program;

cleanup:
        cge_free(shaderbuf);
        arrfree(sources);

        return result;
}

cge_shader *cge_shader_create(const char *vertex_src, const char *fragment_src)
{
        struct cge_shader *shader = malloc(sizeof(*shader));
        shader->id = glCreateProgram();

        struct shader_source sources[] = {
                (struct shader_source){ .type = VERTEX_SHADER, .source = (char *)vertex_src },
                (struct shader_source){ .type = FRAGMENT_SHADER, .source = (char *)fragment_src }
        };

        create_shader(shader->id, sources, countof(sources));
        return shader;
}

cge_shader *cge_shader_create_old(const char *vertex_src, const char *fragment_src)
{
        // Create an empty vertex shader handle
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);

        // Send the vertex shader source code to GL
        // Note that std::string's .c_str is NULL character terminated.
        const GLchar *source = (const GLchar *)vertex_src;
        glShaderSource(vertexShader, 1, &source, 0);

        // Compile the vertex shader
        glCompileShader(vertexShader);

        GLint isCompiled = 0;
        glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
                GLint maxLength = 0;
                glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);

                // The maxLength includes the NULL character
                GLchar info_log[maxLength];
                glGetShaderInfoLog(vertexShader, maxLength, &maxLength, info_log);

                // We don't need the shader anymore.
                glDeleteShader(vertexShader);

                // Use the infoLog as you see fit.
                CGE_LOG_ERROR("Vertex compilation error: %s", info_log);
                CGE_ASSERT(false, "Vertex compilation failed!");
                return NULL;
        }

        // Create an empty fragment shader handle
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        // Send the fragment shader source code to GL
        // Note that std::string's .c_str is NULL character terminated.
        source = (const GLchar *)fragment_src;
        glShaderSource(fragmentShader, 1, &source, 0);

        // Compile the fragment shader
        glCompileShader(fragmentShader);

        glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
                GLint maxLength = 0;
                glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);

                // The maxLength includes the NULL character
                GLchar info_log[maxLength];
                glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, info_log);

                // We don't need the shader anymore.
                glDeleteShader(fragmentShader);
                // Either of them. Don't leak shaders.
                glDeleteShader(vertexShader);

                CGE_LOG_ERROR("Fragment compilation error: %s", info_log);
                CGE_ASSERT(false, "Shader compilation failed!");
                return NULL;
        }

        // Vertex and fragment shaders are successfully compiled.
        // Now time to link them together into a program.
        // Get a program object.
        GLuint program = glCreateProgram();

        // Attach our shaders to our program
        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);

        // Link our program
        glLinkProgram(program);

        // Note the different functions here: glGetProgram* instead of glGetShader*.
        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
        if (isLinked == GL_FALSE) {
                GLint maxLength = 0;
                glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

                // The maxLength includes the NULL character
                GLchar info_log[maxLength];
                glGetProgramInfoLog(program, maxLength, &maxLength, info_log);

                // We don't need the program anymore.
                glDeleteProgram(program);
                // Don't leak shaders either.
                glDeleteShader(vertexShader);
                glDeleteShader(fragmentShader);

                // Use the infoLog as you see fit.

                CGE_LOG_ERROR("Shader link error: %s", info_log);
                CGE_ASSERT(false, "Shader linking failed!");
                return NULL;
        }

        // Always detach shaders after a successful link.
        glDetachShader(program, vertexShader);
        glDetachShader(program, fragmentShader);

        cge_shader *self = malloc(sizeof(cge_shader));
        self->id = program;
        return self;
}

void cge_shader_destroy(cge_shader *self)
{
        glDeleteProgram(self->id);
}

void cge_shader_bind(const cge_shader *self)
{
        glUseProgram(self->id);
}

void cge_shader_unbind(void)
{
        glUseProgram(0);
}

void cge_shader_set_int(const cge_shader *self, const char *name, int num)
{
        GLint location = glGetUniformLocation(self->id, name);
        if (location < 0) {
                CGE_ASSERT(false, "Name does not correspond to an active uniform");
                return;
        }

        glUniform1i(location, num);
}

void cge_shader_set_mat4(const cge_shader *self, const char *name, const mat4 matrix)
{
        GLint location = glGetUniformLocation(self->id, name);
        if (location < 0) {
                CGE_ASSERT(false, "Name does not correspond to an active uniform");
                return;
        }

        glUniformMatrix4fv(location, 1, GL_FALSE, (const float *)matrix);
}

void cge_shader_set_vec4(const cge_shader *self, const char *name, const vec4 uniform)
{
        GLint location = glGetUniformLocation(self->id, name);
        if (location < 0) {
                CGE_ASSERT(false, "Name does not correspond to an active uniform");
                return;
        }

        glUniform4fv(location, 1, (const float *)uniform);
}

void cge_shader_set_vec3(const cge_shader *self, const char *name, const vec3 uniform)
{
        GLint location = glGetUniformLocation(self->id, name);
        if (location < 0) {
                CGE_ASSERT(false, "Name does not correspond to an active uniform");
                return;
        }

        glUniform3fv(location, 1, (const float *)uniform);
}
