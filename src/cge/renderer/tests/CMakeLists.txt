add_executable(renderer_suite renderer_test.c)

target_link_libraries(renderer_suite PRIVATE ${PROJECT_NAME} Unity fff)

add_test(NAME renderer_suite COMMAND renderer_suite)
