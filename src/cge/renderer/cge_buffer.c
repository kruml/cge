#include "cge/cge_buffer.h"
#include "cge/cge_core.h"

#include <string.h>
#include <glad/glad.h>

struct cge_bufferlayout {
        cge_bufferelement_t *elements;
        u32 count;
        u32 stride;
};

static inline void calculate_offset_and_stride(cge_bufferlayout_t *layout)
{
        // for each element, calc offset
        u32 offset = 0;
        for (u32 i = 0; i < layout->count; i++) {
                cge_bufferelement_t *element = &layout->elements[i];
                element->offset = offset;
                offset += element->size;
                layout->stride += element->size;
        }
}

cge_bufferlayout_t *cge_bufferlayout_new(cge_bufferelement_t *elements, u32 count)
{
        cge_bufferlayout_t *layout = malloc(sizeof(struct cge_bufferlayout));
        if (!layout)
                return NULL;

        layout->stride = 0;
        layout->count = count;
        layout->elements = malloc(sizeof(cge_bufferelement_t) * count);
        if (!layout->elements) {
                free(layout);
                return NULL;
        }

        memcpy(layout->elements, elements, (sizeof(cge_bufferelement_t) * count));
        calculate_offset_and_stride(layout);

        return layout;
}

void cge_bufferlayout_destroy(cge_bufferlayout_t *layout)
{
        free(layout->elements);
        layout->elements = NULL;
        free(layout);
}

u32 cge_bufferlayout_stride(const cge_bufferlayout_t *layout)
{
        return layout->stride;
}

cge_bufferlayout_iter_t cge_bufferlayout_start(const cge_bufferlayout_t *layout)
{
        if (layout == NULL)
                return NULL;

        return layout->elements;
}

cge_bufferlayout_iter_t cge_bufferlayout_end(const cge_bufferlayout_t *layout)
{
        if (layout == NULL)
                return NULL;

        return (layout->elements + (layout->count - 1));
}

cge_bufferlayout_iter_t cge_bufferlayout_next(cge_bufferlayout_iter_t iter)
{
        if (iter == NULL)
                return NULL;

        return ++iter;
}

struct cge_vertexbuf {
        u32 renderer_id;
        cge_bufferlayout_t *layout;
};

cge_vbuf_t *cge_vbuf_new(f32 *vertices, i32 size)
{
        cge_vbuf_t *buf = malloc(sizeof(struct cge_vertexbuf));

        glCreateBuffers(1, &buf->renderer_id);
        glBindBuffer(GL_ARRAY_BUFFER, buf->renderer_id);
        glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);

        return buf;
}

void cge_vbuf_destroy(cge_vbuf_t *buf)
{
        glDeleteBuffers(1, &buf->renderer_id);
        free(buf);
}

void cge_vbuf_bind(const cge_vbuf_t *buf)
{
        glBindBuffer(GL_ARRAY_BUFFER, buf->renderer_id);
}

void cge_vbuf_unbind(void)
{
        glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void cge_vbuf_set_layout(cge_vbuf_t *buf, cge_bufferlayout_t *layout)
{
        buf->layout = layout;
}

cge_bufferlayout_t *cge_vbuf_get_layout(const cge_vbuf_t *buf)
{
        return buf->layout;
}

struct cge_indexbuf {
        u32 renderer_id;
        u32 count;
};

cge_ibuf_t *cge_ibuf_new(u32 *indices, u32 count)
{
        cge_ibuf_t *buf = malloc(sizeof(struct cge_indexbuf));
        buf->count = count;
        glCreateBuffers(1, &buf->renderer_id);
        glBindBuffer(GL_ARRAY_BUFFER, buf->renderer_id);
        glBufferData(GL_ARRAY_BUFFER, count * sizeof(uint32_t), indices, GL_STATIC_DRAW);
        return buf;
}

void cge_ibuf_destroy(cge_ibuf_t *buf)
{
        glDeleteBuffers(1, &buf->renderer_id);
        free(buf);
}
void cge_ibuf_bind(const cge_ibuf_t *buf)
{
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buf->renderer_id);
}

void cge_ibuf_unbind(void)
{
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

u32 cge_ibuf_count(const cge_ibuf_t *buf)
{
        return buf->count;
}

#define MAX_VERTEXBUFS (10)

struct cge_vertexarray {
        u32 renderer_id;
        cge_vbuf_t *vertex_buffers[MAX_VERTEXBUFS];
        u8 vertex_buffer_count;
        cge_ibuf_t *index_buffer;
};

cge_varray_t *cge_varray_new(void)
{
        cge_varray_t *array = malloc(sizeof(struct cge_vertexarray));
        array->vertex_buffer_count = 0;
        glCreateVertexArrays(1, &array->renderer_id);
        return array;
}

void cge_varray_destroy(cge_varray_t *buf)
{
        for (int i = 0; i < MAX_VERTEXBUFS; i++) {
                if (buf->vertex_buffers[i] != NULL)
                        free(buf->vertex_buffers[i]);
        }

        if (buf->index_buffer != NULL)
                free(buf->index_buffer);

        free(buf);
}

void cge_varray_bind(const cge_varray_t *buf)
{
        glBindVertexArray(buf->renderer_id);
}

void cge_varray_unbind(void)
{
        glBindVertexArray(0);
}

cge_status_t cge_varray_add_vbuf(cge_varray_t *array, cge_vbuf_t *buf)
{
        if (buf == NULL || array == NULL)
                return CGE_PARAM_ERROR;
        if (array->vertex_buffer_count >= MAX_VERTEXBUFS) {
                CGE_ASSERT(false, "Max vertex arrays hit!");
                return CGE_FAIL;
        }

        cge_varray_bind(array);
        cge_vbuf_bind(buf);
        cge_bufferlayout_t *layout = cge_vbuf_get_layout(buf);
        int index = 0;
        for_each_bufferelement(layout, cge_bufferlayout_end(layout))
        {
                glEnableVertexAttribArray(index);
                glVertexAttribPointer(index, cge_bufferelement_component_count(iter),
                                      cge_shader_data_type_to_opengl(iter->data_type),
                                      (iter->normalized ? GL_TRUE : GL_FALSE), cge_bufferlayout_stride(layout),
                                      (const void *)iter->offset);
                index++;
        }

        array->vertex_buffers[array->vertex_buffer_count] = buf;

        return CGE_OK;
}

cge_status_t cge_varray_set_ibuf(cge_varray_t *array, cge_ibuf_t *buf)
{
        if (array == NULL || buf == NULL) {
                CGE_ASSERT(false, "array or buffer argument NULL!");
                return CGE_PARAM_ERROR;
        }

        cge_varray_bind(array);
        cge_ibuf_bind(buf);
        array->index_buffer = buf;

        return CGE_OK;
}

cge_ibuf_t *cge_varray_get_ibuf(const cge_varray_t *array)
{
        return array->index_buffer;
}
