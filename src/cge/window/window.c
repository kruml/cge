#include "cge_log.h"
#include <GLFW/glfw3.h>
#include <string.h>

#include "cge_assert.h"
#include "cge_core.h"
#include "cge_event.h"
#include "cge_window.h"

static bool isGlfwInitialized = false;

struct cge_window {
        int height;
        int width;
        bool fullscreen;
        bool vsync;
        char *title;
        GLFWwindow *native_window;
};

static void windowresize_fn(GLFWwindow *window, int width, int height)
{
        cge_window_t *self = (cge_window_t *)glfwGetWindowUserPointer(window);
        self->width = width;
        self->height = height;

        CGE_LOG_DEBUG("Resize cb: %d %d", width, height);
        cge_event_publish(&WINDOWRESIZE_EVENT(width, height));
}

static void windowmaximize_fn(GLFWwindow *window, int maximized)
{
        return;
        (void)maximized;
        int width = 0;
        int height = 0;
        glfwGetWindowSize(window, &width, &height);
        CGE_LOG_DEBUG("Maximized size: %d %d", width, height);
        windowresize_fn(window, width, height);
}

static void mousepress_fn(GLFWwindow *window, int button, int action, int mods)
{
        (void)window;
        (void)mods;

        switch (action) {
        case GLFW_PRESS: {
                cge_event_publish(&MOUSEPRESS_EVENT(button, true));
                break;
        }
        case GLFW_RELEASE: {
                cge_event_publish(&MOUSEPRESS_EVENT(button, false));
                break;
        }
        }
}

static void keypress_fn(GLFWwindow *window, int key, int scancode, int action, int mods)
{
        (void)window;
        (void)mods;
        (void)scancode;

        switch (action) {
        case GLFW_PRESS: {
                cge_event_publish(&KEYPRESS_EVENT(key, true, false));
                break;
        }

        case GLFW_RELEASE: {
                cge_event_publish(&KEYPRESS_EVENT(key, false, false));
                break;
        }

        case GLFW_REPEAT: {
                cge_event_publish(&KEYPRESS_EVENT(key, true, true));
                break;
        }

        default: CGE_ASSERT(true, "GLFW Key action not handled!");
        }
}

static void mousescroll_fn(GLFWwindow *window, double xoffset, double yoffset)
{
        (void)window;
        cge_event_publish(&MOUSESCROLL_EVENT(xoffset, yoffset));
}

static void mousemotion_fn(GLFWwindow *window, double xpos, double ypos)
{
        (void)window;
        cge_event_publish(&MOUSEMOTION_EVENT(xpos, ypos));
}

static void glfwerr_fn(int error_code, const char *description)
{
        printf("GLFW Error %d - %s\n", error_code, description);
}

static void windowclose_fn(GLFWwindow *window)
{
        (void)window;
        cge_event_publish(&WINDOWCLOSED_EVENT);
}

cge_window_t *cge_win_new(void)
{
        cge_window_t *self = malloc(sizeof(cge_window_t));
        CGE_ASSERT_NOT_NULL(self, "Creating window failed!");

        self->height = 0;
        self->width = 0;
        self->fullscreen = false;
        self->vsync = false;
        self->title = NULL;
        self->native_window = NULL;

        return self;
}

void set_vsync(cge_window_t *window, bool vsync)
{
        CGE_ASSERT_NOT_NULL(window, "");
        if (vsync) {
                glfwSwapInterval(1);
        } else {
                glfwSwapInterval(0);
        }

        window->vsync = vsync;
}

cge_status_t cge_win_init(cge_window_t *self, int height, int width, bool vsync, bool fullscreen, const char *title)
{
        self->width = width;
        self->height = height;
        self->vsync = vsync;
        self->fullscreen = fullscreen;

        int title_len = (int)strlen(title);
        self->title = malloc(title_len);
        strncpy(self->title, title, title_len);

        if (!isGlfwInitialized) {
                int success = glfwInit();
                if (success == GLFW_FALSE) {
                        CGE_ASSERT(true, "GLFW Init failed");
                        return CGE_FAIL;
                }

                CGE_ASSERT(success == GLFW_TRUE, "GLFW Init failed");
                isGlfwInitialized = true;

                // Decide GL+GLSL versions
                glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
                glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
                glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
                glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);

                // just an extra window hint for resize
                glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        }

        self->native_window = glfwCreateWindow(self->width, self->height, self->title, NULL, NULL);
        glfwMakeContextCurrent(self->native_window);

        glfwSwapInterval(1);

        // check opengl version sdl uses
        printf("opengl version: %s\n", (char *)glGetString(GL_VERSION));

        set_vsync(self, vsync);

        glfwSetWindowUserPointer(self->native_window, self);
        glfwSetErrorCallback(glfwerr_fn);
        glfwSetWindowSizeCallback(self->native_window, windowresize_fn);
        glfwSetWindowMaximizeCallback(self->native_window, windowmaximize_fn);
        glfwSetKeyCallback(self->native_window, keypress_fn);
        glfwSetMouseButtonCallback(self->native_window, mousepress_fn);
        glfwSetScrollCallback(self->native_window, mousescroll_fn);
        glfwSetCursorPosCallback(self->native_window, mousemotion_fn);
        glfwSetWindowCloseCallback(self->native_window, windowclose_fn);
        return CGE_OK;
}

void cge_win_update(cge_window_t *window)
{
        glfwPollEvents();
        glfwSwapBuffers(window->native_window);
}

void cge_win_destroy(cge_window_t **self)
{
        cge_window_t *self_deref = *self;
        free(self_deref->title);
        glfwDestroyWindow(self_deref->native_window);
        free(self_deref);
        self_deref = NULL;
}

void *cge_win_native(cge_window_t *window)
{
        return window->native_window;
}

int cge_win_height(cge_window_t *window)
{
        return window->height;
}

int cge_win_width(cge_window_t *window)
{
        return window->width;
}
