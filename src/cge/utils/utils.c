#include "utils.h"

char *cge_strcat(const char *str1, const char *str2)
{
        char *newstr = NULL;
        size_t n = 0;

        if (str1)
                n += strlen(str1);
        if (str2)
                n += strlen(str2);

        if ((str1 || str2) && (newstr = malloc(n + 1)) != NULL) {
                *newstr = '\0';
                if (str1)
                        strcpy(newstr, str1);
                if (str2)
                        strcat(newstr, str2);
        }

        return newstr;
}

char *cge_assetpath(const char *relpath)
{
        return cge_strcat(DEFAULT_ASSETS_PATH, relpath);
}
