#ifndef CGE_UTILS_H
#define CGE_UTILS_H

#include <stdlib.h>
#include <string.h>

/**
 * @brief Safely concatenates two strings
 *
 * Given two strings, this functions will safely concatenate the strings and return.
 * Handles NULL strings.
 *
 * @param str1: First string. Can be NULL
 * @param str2: Seconds string concatenated onto @ref str1. Can be NULL
 * @return concatenated string
 *  */
char *cge_strcat(const char *str1, const char *str2);

/**
 * @brief Creates and absolute path to asset file
 *
 * Given a relative path to an asset, this function will concatenate it with
 * DEFAULT_ASSET_PATH.
 *
 * @todo Let user set custom path as env variable
 * @example char* assetpath = cge_assetpath("shaders/example.glsl");
 *
 * @param relpath: relative path to asset
 */
char *cge_assetpath(const char *relpath);

#endif // CGE_UTILS_H
