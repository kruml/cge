#include "cge/cge_camera_controller.h"
#include "cge/cge_input.h"
#include "cge/cge_assert.h"
#include "cge_camera.h"
#include "cge_event.h"
#include "cge_log.h"
#include "cglm/vec2.h"

static struct cge_camortho_ctrl _config = { 0 };
static bool _user_configured = false;

static void on_resize(const struct cge_event *event)
{
        CGE_ASSERT_EVENT(CGE_WINDOWRESIZE, event->id);
        const struct cge_windowresize *event_data = &event->windowresize;
        CGE_LOG_DEBUG("Resize %f %f", event_data->size[0], event_data->size[1])
        cge_camortho_update_projection(&_config.camera, event_data->size[0], event_data->size[1]);
}

static void on_scroll(const struct cge_event *event)
{
        CGE_ASSERT_EVENT(CGE_MOUSESCROLL, event->id);
}

static void on_key(const struct cge_event *event)
{
        CGE_ASSERT_EVENT(CGE_KEYEVENT, event->id);
        const struct cge_keypress *event_data = &event->keypress;
        switch (event_data->key) {
        case CGE_KEY_LEFT: _config.velocity[0] = event_data->pressed ? _config.move_speed : 0.0F; break;
        case CGE_KEY_RIGHT: _config.velocity[0] = event_data->pressed ? -_config.move_speed : 0.0F; break;
        case CGE_KEY_UP: _config.velocity[1] = event_data->pressed ? -_config.move_speed : 0.0F; break;
        case CGE_KEY_DOWN: _config.velocity[1] = event_data->pressed ? _config.move_speed : 0.0F; break;
        default: break;
        }
}

void cge_camortho_ctrl_cfg(const struct cge_camortho_ctrl *config)
{
        _config = *config;
        _user_configured = true;
}

const struct cge_camortho *cge_camortho_ctrl_get_cam(void)
{
        return &_config.camera;
}

void cge_camortho_ctrl_attach(void)
{
        if (_user_configured)
                return;

        CGE_LOG_DEBUG("Default initializing cam controller!");

        _config = (struct cge_camortho_ctrl){ .camera = cge_camortho_init(1280.0f, 720.0f),
                                              .velocity = { 0.0F, 0.0F, 0.0F },
                                              .move_speed = 0.4F,
                                              .zoom_speed = 1.0F,
                                              .rotation_speed = 1.0F };

        cge_event_subscribe(CGE_KEYEVENT, on_key);
        cge_event_subscribe(CGE_MOUSESCROLL, on_scroll);
        cge_event_subscribe(CGE_WINDOWRESIZE, on_resize);
}

void cge_camortho_ctrl_update(f32 dt_seconds)
{
        if ((_config.velocity[0] == 0.0F) && (_config.velocity[1] == 0.0F))
                return;

        vec2 normalized_velocity = { _config.velocity[0], _config.velocity[1] };
        glm_vec2_normalize(normalized_velocity);

        _config.camera.position[0] += _config.move_speed * normalized_velocity[0] * dt_seconds;
        _config.camera.position[1] += _config.move_speed * normalized_velocity[1] * dt_seconds;
        cge_camortho_update_view_transform(&_config.camera);
}
