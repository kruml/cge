#include "cge_app.h"
#include "cge/cge_event.h"
#define CIMGUI_DEFINE_ENUMS_AND_STRUCTS

#include "cge_assert.h"
#include "cge_gui.h"
#include "cge_log.h"
#include "cge_window.h"

#include <cimgui.h>
#include <cimgui_impl.h>
#include <stdio.h>

#ifdef _MSC_VER
#include <windows.h>
#endif
#include <GLFW/glfw3.h>

#ifdef IMGUI_HAS_IMSTR
#define igBegin igBegin_Str
#define igSliderFloat igSliderFloat_Str
#define igCheckbox igCheckbox_Str
#define igColorEdit3 igColorEdit3_Str
#define igButton igButton_Str
#endif

#if __APPLE__
// GL 3.2 Core + GLSL 150
#define GLSL_VER "#version 150"
#else
#define GLSL_VER "#version 130"
#endif
#define OPENGL_VER "#version 330 core"

static bool show_demo = true;

static void key_handler(const struct cge_event *event)
{
        CGE_ASSERT_EVENT(CGE_KEYEVENT, event->id);
        switch (event->keypress.key) {
        case CGE_KEY_D:
                if (event->keypress.pressed == 0) {
                        show_demo = !show_demo;
                        CGE_LOG_DEBUG("Showing demo window: %d", show_demo);
                }
                break;
        default: break;
        }
}

void gui_update(f32 dt_seconds)
{
        if (!show_demo) {
                return;
        }

        ImGuiIO *io = igGetIO();

        // TODO: Probably nicer to let this happen with events instead.
        io->DisplaySize = (ImVec2){ cge_get_window_w(), cge_get_window_h() };

        io->DeltaTime = dt_seconds;

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        igNewFrame();

        igShowDemoWindow(&show_demo);
        igRender();
        ImGui_ImplOpenGL3_RenderDrawData(igGetDrawData());
}

void gui_destroy(void)
{
        CGE_LOG_DEBUG("Destroying GUI");
        igDestroyContext(NULL);
}

void gui_init(void)
{
        CGE_LOG_DEBUG("Initializing GUI");

        // setup imgui
        igCreateContext(NULL);
        igStyleColorsDark(NULL);

        ImGuiIO *ioptr = igGetIO();
        ioptr->ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
        ioptr->ConfigFlags |= ImGuiBackendFlags_HasMouseCursors;
        ioptr->ConfigFlags |= ImGuiBackendFlags_HasSetMousePos;
        GLFWwindow *win = (GLFWwindow *)cge_win_native(cge_get_window());
        CGE_ASSERT_NOT_NULL(win, "Native window not initialized!");

        if (!ImGui_ImplGlfw_InitForOpenGL(win, true))
                CGE_ASSERT(true, "Failed to init GLFW for OpenGL");

        if (!ImGui_ImplOpenGL3_Init(OPENGL_VER))
                CGE_ASSERT(true, "Failed to initialize ImGUI OpenGL3 implementation");

        cge_event_subscribe(CGE_KEYEVENT, key_handler);
}
