#include "cge/cge_assert.h"
#include "cge/cge_input.h"
#include "cge/cge_event.h"
#include "cge/cge_keymap.h"

#include <pthread.h>
#include <stdbool.h>

static bool key_pressed[CGE_KEY_LAST] = { 0 };
static pthread_mutex_t key_pressed_mutex = PTHREAD_MUTEX_INITIALIZER;

static void on_key_pressed(const struct cge_event *event)
{
        CGE_ASSERT_EVENT(CGE_KEYEVENT, event->id);

        if (event == NULL)
                return;

        pthread_mutex_lock(&key_pressed_mutex);
        key_pressed[event->keypress.key] = event->keypress.pressed;
        pthread_mutex_unlock(&key_pressed_mutex);
}

bool cge_input_is_key_pressed(enum cge_key key)
{
        if (key <= CGE_KEY_NONE || key >= CGE_KEY_LAST)
                return false;

        pthread_mutex_lock(&key_pressed_mutex);
        bool pressed = key_pressed[key];
        pthread_mutex_unlock(&key_pressed_mutex);

        return pressed;
}

void cge_input_init(void)
{
        cge_event_subscribe(CGE_KEYEVENT, on_key_pressed);
}
