#include "cge_assert.h"
#include "cge_layer.h"
#include "cge_layer_stack.h"
#include "cge_log.h"

struct CgeLayerStack {
        int top;
        int max;
        cge_layer_t layers[MAX_LAYERS];
};

cge_layerstack_t *self = NULL;

bool cge_lstack_push(cge_layerstack_t *stack, cge_layer_t new_layer)
{
        if (stack->top + 1 >= stack->max) {
                CGE_ASSERT(true, "Too many layers registered!");
                return false;
        }

        stack->top++;

        if (stack->top == 0) {
                stack->layers[stack->top] = new_layer;
        } else {
                for (int i = stack->top; i >= 0; i--) {
                        stack->layers[i + 1] = stack->layers[i];
                }
                stack->layers[0] = new_layer;
        }

        if (new_layer.on_attach != NULL)
                new_layer.on_attach();
        return true;
}

bool cge_lstack_pop(cge_layerstack_t *stack)
{
        if (stack->top < 0) {
                return false;
        }

        for (int i = 0; i <= stack->top; i++) {
                stack->layers[i] = stack->layers[i + 1];
        }

        stack->layers[stack->top].on_detach();
        stack->top--;
        return true;
}

bool cge_lstack_push_overlay(cge_layerstack_t *stack, cge_layer_t new_overlay)
{
        if (stack->top + 1 >= stack->max) {
                CGE_ASSERT(true, "Too many layers registered!");
                return false;
        }
        stack->layers[++stack->top] = new_overlay;
        new_overlay.on_attach();
        return true;
}

bool cge_lstack_pop_overlay(cge_layerstack_t *stack)
{
        if (stack->top < 0) {
                return false;
        }

        stack->layers[stack->top].on_detach();
        stack->top--;
        return true;
}

cge_layerstack_t *cge_lstack_new(void)
{
        if (self != NULL) {
                return self;
        }
        self = calloc(1, sizeof(cge_layerstack_t));
        self->top = -1;
        self->max = (sizeof(self->layers) / sizeof(self->layers[0]));
        CGE_ASSERT(MAX_LAYERS == self->max, "Size error, double check everything!");
        return self;
}

void cge_lstack_destroy(cge_layerstack_t *stack)
{
        while (stack->top >= 0) {
                cge_lstack_pop_overlay(stack);
        }
}

CgeLayerIterator cge_lstack_begin(cge_layerstack_t *stack)
{
        return (CgeLayerIterator)&stack->layers[0];
}

CgeLayerIterator cge_lstack_end(cge_layerstack_t *stack, int end_idx)
{
        return (CgeLayerIterator)&stack->layers[end_idx];
}

CgeLayerIterator cge_lstack_next(CgeLayerIterator iter)
{
        return ++iter;
}

void cge_lstack_update(cge_layerstack_t *layers, f32 dt_seconds)
{
        for_each_layer(layers, layers->top)
        {
                if (iter->on_update != NULL) {
                        iter->on_update(dt_seconds);
                }
        }
}
