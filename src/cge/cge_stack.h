#ifndef STACK_H
#define STACK_H

#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#define STACK_DECLARE(type_name, T)                      \
        typedef struct node_##T node_##T;                \
        typedef struct type_name type_name;              \
                                                         \
        type_name *type_name##_create(void);             \
        T type_name##_pop(type_name *stack);             \
        bool type_name##_is_empty(type_name *stack);     \
        void type_name##_destroy(type_name *stack);      \
        bool type_name##_push(type_name *stack, T data); \
        T type_name##_peek(type_name *stack);

#define STACK_DEFINE(type_name, T)                            \
        struct node_##T {                                     \
                T data;                                       \
                node_##T *next;                               \
        };                                                    \
                                                              \
        struct type_name {                                    \
                node_##T *head;                               \
                size_t nodes;                                 \
        };                                                    \
                                                              \
        type_name *type_name##_create(void)                   \
        {                                                     \
                type_name *stack = malloc(sizeof(type_name)); \
                                                              \
                stack->head = NULL;                           \
                stack->nodes = 0;                             \
                return stack;                                 \
        }

T type_name##_pop(type_name *stack)
{
        node_##T *popped = stack->head;
        stack->head = popped->next;
        stack->nodes--;
        T ret = popped->data;
        free(popped);

        return ret;
}

bool type_name##_is_empty(type_name *stack)
{
        return stack->nodes == 0;
}

void type_name##_destroy(type_name *stack)
{
        while (!type_name##_is_empty(stack)) {
                type_name##_pop(stack);
        }
}
bool type_name##_push(type_name *stack, T data)
{
        node_##T *new_node = malloc(sizeof(node_##T));
        if (new_node == NULL) {
                return false;
        }

        new_node->data = data;
        new_node->next = stack->head;
        stack->head = new_node;
        stack->nodes++;
        return true;
}

T type_name##_peek(type_name *stack)
{
        T ret = stack->head->data;
        return ret;
}

#endif // STACK_H
