#include "cge/cge_app.h"
#include "cge/cge_assert.h"
#include "cge/cge_core.h"
#include "cge/cge_event.h"
#include "cge/cge_gui.h"
#include "cge/cge_input.h"
#include "cge/cge_layer.h"
#include "cge/cge_layer_stack.h"
#include "cge/cge_renderer.h"
#include "cge/cge_window.h"

#include "ecs/decs.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

struct app_data {
        cge_window_t *window;
        cge_layerstack_t *layer_stack;
};

static bool _running = true;
static bool _minimized = false;
static f32 _last_frame_time_seconds = 0;
static struct app_data _data = { .window = NULL, .layer_stack = NULL };

static void handle_window_close(const struct cge_event *event)
{
        CGE_ASSERT_EVENT(CGE_WINDOWCLOSED, event->id);
        _running = false;
}

static void handle_window_resize(const struct cge_event *event)
{
        CGE_ASSERT_EVENT(CGE_WINDOWRESIZE, event->id);
        const struct cge_windowresize *event_data = &event->windowresize;
        if ((event_data->size[0] == 0) || (event_data->size[1] == 0)) {
                _minimized = true;
                return;
        }

        _minimized = false;
        cge_renderer_on_winresize((u32)event_data->size[0], (u32)event_data->size[1]);
}

static cge_status_t setup_window(const struct cge_settings *settings, cge_window_t **window)

{
        *window = cge_win_new();
        if (window == NULL)
                return CGE_FAIL;

        cge_win_init(*window, settings->window_h, settings->window_w, settings->window_vsync,
                     settings->window_fullscreen, settings->window_title);

        gladLoadGL();
        return CGE_OK;
}

cge_status_t cge_init(const struct cge_settings *settings)
{
        CHECK(setup_window(settings, &_data.window));
        _data.layer_stack = cge_lstack_new();
        cge_input_init();
        cge_renderer_init();
        cge_event_subscribe(CGE_WINDOWCLOSED, handle_window_close);
        cge_event_subscribe(CGE_WINDOWRESIZE, handle_window_resize);
        cge_push_overlay(gui_layer);
        return CGE_OK;
}

cge_status_t cge_push_layer(cge_layer_t layer)
{
        if (cge_lstack_push(_data.layer_stack, layer) == false)
                return CGE_FAIL;

        return CGE_OK;
}

cge_status_t cge_push_overlay(cge_layer_t layer)
{
        if (cge_lstack_push_overlay(_data.layer_stack, layer) == false)
                return CGE_FAIL;

        return CGE_OK;
}

cge_status_t cge_pop_layer(void)
{
        if (cge_lstack_pop(_data.layer_stack) == false)
                return CGE_FAIL;

        return CGE_OK;
}

int cge_get_window_h(void)
{
        return cge_win_height(_data.window);
}

int cge_get_window_w(void)
{
        return cge_win_width(_data.window);
}

cge_window_t *cge_get_window(void)
{
        return _data.window;
}

void cge_run(void)
{
        while (_running) {
                f32 time = (f32)glfwGetTime();
                f32 timestep = time - _last_frame_time_seconds;
                _last_frame_time_seconds = time;
                cge_event_dispatch_all();
                if (!_minimized) {
                        cge_lstack_update(_data.layer_stack, timestep);
                        cge_win_update(_data.window);
                }
        }

        cge_win_destroy(&_data.window);
        cge_lstack_destroy(_data.layer_stack);
        CGE_LOG_INFO("Closing CGE. Goodbye!");
}
