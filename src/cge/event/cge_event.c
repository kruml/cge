#include "cge/cge_event.h"
#include "cge/cge_core.h"
#include "cge/cge_assert.h"
#include "cge_log.h"

#define MAX_LISTENERS (10)
#define MAX_EVENTS (512)

struct event_queue {
        struct cge_event buf[MAX_EVENTS];
        u16 head;
        u16 tail;
        u16 count;
        const u16 size;
};

struct event_listeners {
        cge_event_listener buf[CGE_EVENTCNT][MAX_LISTENERS];
        u32 count[CGE_EVENTCNT];
};

static struct event_queue _events = { .size = MAX_EVENTS };
static struct event_listeners _listeners = { 0 };

static u32 get_eventidx(enum cge_event_id event)
{
        u32 event_num = 0xFFFF;
        for (int i = 0; i < CGE_EVENTCNT; i++) {
                if (is_bit_set(event, i)) {
                        event_num = i;
                        break;
                }
        }

        return event_num;
}

void cge_event_subscribe(enum cge_event_id type, cge_event_listener listener)
{
        for (int i = 0; i < CGE_EVENTCNT; i++) {
                if (is_bit_set(type, i)) {
                        _listeners.buf[i][_listeners.count[i]] = listener;
                        _listeners.count[i]++;
                }
        }
}

// how is data controlled
// currently the data will go out of scope
// we do not know data size so can't copy
// can add data size
//
void cge_event_dispatch_all(void)
{
        while (_events.count > 0) {
                struct cge_event *event = &_events.buf[_events.tail];
                u16 event_idx = get_eventidx(_events.buf[_events.tail].id);
                for (u32 i = 0; i < _listeners.count[event_idx]; i++) {
                        cge_event_listener listener = _listeners.buf[event_idx][i];
                        if (listener == NULL) {
                                CGE_ASSERT(true, "Registered listener is NULL!");
                                continue;
                        }
                        listener(event);
                }

                _events.tail = ((_events.tail + 1) & (MAX_EVENTS - 1));
                _events.count--;
        }
}

void cge_event_publish(struct cge_event *event)
{
        if (_events.count == _events.size) {
                CGE_ASSERT(true, "Event queue full!");
                CGE_LOG_ERROR("Event queue full!");
                return;
        }

        _events.buf[_events.head] = *event;
        _events.head = ((_events.head + 1) & (MAX_EVENTS - 1));
        _events.count++;
}
