#include <cge/cge.h>

#include "test_layer.h"
#include "game_layer.h"

static const struct cge_settings settings = { .window_h = 720,
                                              .window_w = 1280,
                                              .window_title = "CGE",
                                              .window_fullscreen = false,
                                              .window_vsync = true };

int main(void)
{
        CHECK(cge_init(&settings));
        //CHECK(cge_push_layer(inputdbg_layer));
        CHECK(cge_push_layer(game_layer));

        cge_run();
}
