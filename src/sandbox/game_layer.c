#include "game_layer.h"
#include "cge/cge_window.h"
#include "cge/cge_app.h"
#include "cge/cge_camera.h"
#include "cge/cge_camera_controller.h"
#include "cge/cge_buffer.h"
#include "cge/cge_shader.h"
#include "cge/cge_renderer.h"
#include "cge_core.h"
#include "cge_log.h"
#include "cge_texture.h"

#include "ecs/decs.h"

#include "ecs/types.h"
#include <glad/glad.h>

static cge_window_t *window_ref = NULL;
static const struct cge_camortho *camera = NULL;
static struct cge_texture2d checkerboard_texture = { 0 };
static World *my_world = NULL;
static EntityId player;

typedef struct vector_t {
        uint32_t x, y;
} Vector;

typedef struct velocity_t {
        uint8_t x, y;
} Velocity;

typedef struct quad_t {
        vec2 size;
        vec4 color;
} Quad;

DECS_COMPONENT_DECLARE(Vector)
DECS_COMPONENT_DECLARE(Velocity)
DECS_COMPONENT_DECLARE(Quad)

DecsStatus rendersys_fn(float dt)
{
        /* LOG_DEBUG("CALLBACK FUNCTION 1 TEST %f!\n", dt); */
        Signature system_signature_velocity = (DECS_COMPONENT_ID(Vector) | DECS_COMPONENT_ID(Quad));
        Quad *quad;
        Vector *vec;
        EntityIterator it = decs_EntityIteratorBegin(my_world);
        EntityIterator end = decs_EntityIteratorEnd(my_world);

        cge_renderer_begin_scene(cge_camortho_ctrl_get_cam());
        {
                for (; it <= end; it = decs_EntityIteratorNext(it)) {
                        if (!IS_FLAG_SET(system_signature_velocity, it->signature))
                                continue;

                        quad = DECS_COMPONENT_GET(Quad, my_world, it->id);
                        vec = DECS_COMPONENT_GET(Vector, my_world, it->id);
                        assert(quad != NULL);
                        assert(vec != NULL);
                        cge_renderer_draw_quad2d((vec2){ vec->x, vec->y }, quad->size, quad->color);
                }
        }
        cge_renderer_end_scene();

        return kDecsSuccess;
}
void game_layer_on_attach(void)
{
        DECS_COMPONENT_DEFINE(Vector)
        DECS_COMPONENT_DEFINE(Velocity)
        DECS_COMPONENT_DEFINE(Quad)

        my_world = decs_WorldCreate("Krummis world");
        player = decs_CreateEntity(my_world);

        Vector vect1 = { 0, 0 };
        DECS_COMPONENT_ADD(Vector, my_world, vect1, player);

        Quad quad = { .size = { 0.9f, 0.9f }, .color = { 0.7f, 0.3f, 0.4f, 1.0f } };
        DECS_COMPONENT_ADD(Quad, my_world, quad, player);

        for (int i = 0; i < 100; i++) {
                for (int j = 0; j < 100; j++) {
                        EntityId entity = decs_CreateEntity(my_world);
                        vect1 = (Vector){ i - 5, j - 5 };
                        DECS_COMPONENT_ADD(Vector, my_world, vect1, entity);
                        DECS_COMPONENT_ADD(Quad, my_world, quad, entity);
                }
        }

        window_ref = cge_get_window();
        checkerboard_texture = cge_texture_create("/textures/Checkerboard.png");

        Signature system_signature_velocity = (DECS_COMPONENT_ID(Vector) | DECS_COMPONENT_ID(Quad));
        decs_SystemRegister(my_world, rendersys_fn, system_signature_velocity);

        cge_push_layer(camortho_ctrl_layer);
}

void game_layer_on_detatch(void)
{
        return;
}

void game_layer_on_update(f32 dt_seconds)
{
        decs_Update(my_world, dt_seconds);
}

void game_layer_set_window(cge_window_t *window)
{
        window_ref = window;
}
