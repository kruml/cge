#ifndef SANDBOX_GAME_LAYER_H_
#define SANDBOX_GAME_LAYER_H_

#include "cge/cge_layer.h"

void game_layer_on_attach(void);
void game_layer_on_detatch(void);
void game_layer_on_update(f32 dt_seconds);

static const cge_layer_t game_layer = { .on_attach = game_layer_on_attach,
                                        .on_detach = game_layer_on_detatch,
                                        .on_update = game_layer_on_update };

#endif //SANDBOX_GAME_LAYER_H_
