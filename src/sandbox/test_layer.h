#include <stdio.h>
#include <stdbool.h>

#include "cge/cge_assert.h"
#include "cge/cge_keymap.h"
#include "cge/cge_core.h"
#include "cge/cge_layer.h"
#include "cge/cge_event.h"

#include <cglm/cglm.h>

static void layerOnAttach(void);
static void layerOnDetach(void);
static void layerOnUpdate(f32 dt_seconds);

static const cge_layer_t inputdbg_layer = { .on_attach = layerOnAttach,
                                            .on_detach = layerOnDetach,
                                            .on_update = layerOnUpdate };

static void input_handler(const struct cge_event *event)
{
        switch (event->id) {
        case CGE_WINDOWRESIZE: {
                const struct cge_windowresize *event_data = &event->windowresize;
                printf("Window resize [%f,%f]\n", event_data->size[0], event_data->size[1]);
                break;
        }
        case CGE_KEYEVENT: {
                const struct cge_keypress *event_data = &event->keypress;
                printf("Key %d, pressed %d repeat %d\n", event_data->key, event_data->pressed, event_data->repeat);
                break;
        }
        case CGE_MOUSESCROLL: {
                const struct cge_mousescroll *event_data = &event->mousescroll;
                printf("Scroll offset [%f, %f]\n", event_data->offset[0], event_data->offset[1]);
                break;
        }
        case CGE_MOUSEBUTTON: {
                const struct cge_mousepress *event_data = &event->mousepress;
                printf("Mouse button %d pressed %d\n", event_data->button, event_data->pressed);
                break;
        }
        case CGE_MOUSEMOTION: {
                const struct cge_mousemotion *event_data = &event->mousemotion;
                printf("Layer: Mouse moved [%.2f,%.2f]\n", event_data->position[0], event_data->position[1]);
                break;
        }
        default: CGE_ASSERT(true, "Unhandled event!"); break;
        }
}

static void layerOnAttach(void)
{
        cge_event_subscribe((CGE_KEYEVENT | CGE_MOUSEMOTION | CGE_MOUSEBUTTON | CGE_MOUSEMOTION | CGE_MOUSESCROLL |
                             CGE_WINDOWRESIZE),
                            input_handler);
        //cge_event_sub_window_resize(windowResizeCallback);
        //cgeMouseButtonEventRegisterCb(mouseButtonEvent);
        //cgeMouseScrollEventRegisterCb(mouseScrollCallback);
        //cgeWindowCloseEventRegisterCb(onWindowClose);
        printf("Test layer attached!\n");
}

static void layerOnDetach(void)
{
        printf("Test layer detached!\n");
}

static void layerOnUpdate(f32 dt_seconds)
{
        (void)dt_seconds;
        return;
}
