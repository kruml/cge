#include "game_layer.h"
#include "cge/cge_window.h"
#include "cge/cge_app.h"
#include "cge/cge_camera.h"
#include "cge/cge_camera_controller.h"
#include "cge/cge_buffer.h"
#include "cge/cge_shader.h"
#include "cge/cge_renderer.h"

#include <glad/glad.h>

static cge_window_t *window_ref = NULL;
static const struct cge_camortho *camera = NULL;
static cge_varray_t *vertex_array = NULL;
static cge_shader *shader = NULL;

static const vec4 clear_color = { 0.1F, 0.1F, 0.1F, 1 };

/* clang-format off */
static float vertices[3 * 3] = {
    -0.5F, -0.5F, 0.0F,
     0.5F, -0.5F, 0.0F,
     0.0F,  0.5F, 0.0F
};
/* clang-format on */

void game_layer_on_attach(void)
{
        window_ref = cge_app_get_window();

        cge_app_push_layer(camortho_ctrl_layer);
        shader = cge_shader_create_from_file("/shaders/flat_color.glsl");

        cge_vbuf_t *vertex_buf = cge_vbuf_new(vertices, sizeof(vertices));
        cge_vbuf_bind(vertex_buf);
        {
                cge_bufferelement_t elements[] = {
                        cge_bufferelement_create(CGE_SHADER_DATA_VEC3, "testing"),
                };

                cge_vbuf_set_layout(vertex_buf, cge_bufferlayout_new(elements, countof(elements)));
        }

        unsigned int indices[3] = { 0, 1, 2 };
        cge_ibuf_t *index_buf = cge_ibuf_new(indices, countof(indices));
        cge_ibuf_bind(index_buf);

        vertex_array = cge_varray_new();
        cge_varray_add_vbuf(vertex_array, vertex_buf);
        cge_varray_set_ibuf(vertex_array, index_buf);
}

void game_layer_on_detatch(void)
{
        cge_varray_destroy(vertex_array);
        cge_shader_destroy(shader);
}

void game_layer_on_update(f32 dt_seconds)
{
        cge_renderer_set_clear_color(clear_color);
        cge_renderer_clear();

        cge_renderer_begin_scene(cge_camortho_ctrl_get_cam());
        {
                cge_renderer_submit(shader, vertex_array);
        }
        cge_renderer_end_scene();
}

void game_layer_set_window(cge_window_t *window)
{
        window_ref = window;
}
