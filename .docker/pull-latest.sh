#!/bin/bash

root_dir=`git rev-parse --show-toplevel`
docker_dir=$root_dir/.docker
registry_name=registry.gitlab.com/krum-game-engine/cge/cge-build

source $docker_dir/.env

image_version=${MAJOR_VER}.${MINOR_VER}.${PATCH_VER}


echo "Pulling $registry_name:latest"
docker pull $registry_name:latest
