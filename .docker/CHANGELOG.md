# CGE Build Docker image changelog
## 1.0.2
- Added support for building vcpkg
- Added package fixuid to solve uid and gid errors

## 1.0.1
- Added support for CodeChecker

## 1.0.0
- Building in gitlab ci
