#!/bin/bash

root_dir=`git rev-parse --show-toplevel`
docker_dir=$root_dir/.docker
registry_name=registry.gitlab.com/krum-game-engine/cge/cge-build

source $docker_dir/.env

image_version=${MAJOR_VER}.${MINOR_VER}.${PATCH_VER}

echo "Pushing cge-build:$image_version to $registry_name"
docker build -t $registry_name:$image_version $docker_dir
docker push $registry_name:$image_version

echo "Pushing cge-build:$image_version as cge_build:latest to $registry_name"
docker build -t $registry_name:latest $docker_dir
docker push $registry_name:latest
