#!/bin/bash

root_dir=`git rev-parse --show-toplevel`
docker_dir=$root_dir/.docker

source $docker_dir/.env

image_version=${MAJOR_VER}.${MINOR_VER}.${PATCH_VER}

echo "Building cge-build:$image_version"

docker build $docker_dir -t cge-build:latest
docker build $docker_dir -t cge-build:$image_version
